﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

public class TN_Helper
{
    public static string TeleBotToken { get; set; } = "";
    public static string TeleReceiveID { get; set; } = "";
    public static string BusinessName { get; set; } = "";
    public static string ConnectionString { get; set; } = "";
    public static string PartnerNumber { get; set; } = "";
    public static string MessageUpload { get; set; } = "";

    public static async void SendMessage(string Message)
    {
        var botClient = new Telegram.Bot.TelegramBotClient(TeleBotToken);
        await botClient.SendTextMessageAsync("1870391117", Message);
    }

    public static async Task<List<string>> UploadMultipleAsync(List<IFormFile> files, string Folder)
    {
        var ListResult = new List<string>();

        if (files == null)
            return ListResult;

        var zWebPath = "_FileUpload/" + PartnerNumber + "/" + Folder + "/";
        try
        {
            //upload nhiều file xài code này
            if (files.Count > 1)
            {
                long size = files.Sum(f => f.Length);
                var filePaths = new List<string>();
                foreach (var formFile in files)
                {
                    if (formFile.Length > 0)
                    {
                        // full path to file in temp location
                        var filePath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/" + zWebPath);
                        filePaths.Add(filePath);
                        var fileNameWithPath = string.Concat(filePath, "\\", formFile.FileName);

                        if (File.Exists(filePath))
                        {
                            // If file found, delete it    
                            File.Delete(filePath);
                        }

                        Directory.CreateDirectory(filePath);

                        using var stream = new FileStream(fileNameWithPath, FileMode.Create);
                        await formFile.CopyToAsync(stream);

                        var webPath = "~/" + zWebPath + formFile.FileName;
                        ListResult.Add(webPath);
                    }
                }
            }

        }
        catch (Exception ex)
        {
            MessageUpload = ex.ToString();
        }

        return ListResult;
    }
    public static async Task<string> UploadAsync(IFormFile file, string Folder)
    {
        var zResult = "";

        if (file == null)
            return zResult;

        var zWebPath = "_FileUpload/" + PartnerNumber + "/" + Folder + "/";
        try
        {
            var filePath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/" + zWebPath);
            var fileNameWithPath = string.Concat(filePath, "\\", file.FileName);

            if (File.Exists(filePath))
            {
                // If file found, delete it    
                File.Delete(filePath);
            }
            Directory.CreateDirectory(filePath);

            using var stream = new FileStream(fileNameWithPath, FileMode.Create);
            await file.CopyToAsync(stream);

            zResult = "~/" + zWebPath + file.FileName;
        }
        catch (Exception ex)
        {
            MessageUpload = ex.ToString();
        }

        return zResult;
    }

    public static void DeleteFile(string WebPath)
    {
        var filePath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/" + WebPath);

        if (File.Exists(filePath))
        {
            // If file found, delete it    
            File.Delete(filePath);
        }
    }
}