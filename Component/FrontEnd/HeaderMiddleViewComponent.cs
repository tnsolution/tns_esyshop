﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebSite.Component
{
    [ViewComponent]
    public class HeaderMiddleViewComponent : ViewComponent
    {
        public IViewComponentResult Invoke()
        {
            string Logo = "";
            var zInfo = new Partner_Info(TN_Helper.PartnerNumber);
            if (zInfo.Partner.Logo_Large.Length > 0)
            {
                Logo = zInfo.Partner.Logo_Large;
            }
            else
            {
                Logo = "~/themes/admin/img/logodefault.png";
            }

            ViewBag.Logo = Logo;

            return View("~/Views/Shared/Components/HeaderMiddle.cshtml");
        }
    }
}