﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebSite.Component
{
    [ViewComponent]
    public class Group3ViewComponent : ViewComponent
    {
        public IViewComponentResult Invoke(string ID)
        {
            var zInfo = new Config_Info(ID, TN_Helper.PartnerNumber);
            ViewBag.Title = zInfo.Config.Name;

            var zParent = zInfo.Config.ConfigKey;
            var zList = Config_Data.List(TN_Helper.PartnerNumber, zParent, out _);
           
            return View("~/Views/Shared/Components/Group3.cshtml", zList);
        }
    }
}
