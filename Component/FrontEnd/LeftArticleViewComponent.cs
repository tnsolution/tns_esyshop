﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebSite.Component
{
    [ViewComponent]
    public class LeftArticleViewComponent: ViewComponent
    {
        public IViewComponentResult Invoke(int CategoryKey)
        {
            //các bài viết
            var zList = Article_Data.ListShow(TN_Helper.PartnerNumber, CategoryKey, out _);  
            
            //danh mục
            //Article_Category_Data.List(TN_Helper.PartnerNumber, CategoryKey, out _);

            return View("~/Views/Shared/Components/LeftArticle.cshtml", zList);
        }
    }
}
