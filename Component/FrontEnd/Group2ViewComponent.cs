﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebSite.Component
{
    [ViewComponent]
    public class Group2ViewComponent : ViewComponent
    {
        public IViewComponentResult Invoke(string ID)
        {
            var zInfo = new Config_Info(ID, TN_Helper.PartnerNumber);
            ViewBag.Title = zInfo.Config.Name;

            var zParent = zInfo.Config.ConfigKey;

            var zList = Config_Data.List(TN_Helper.PartnerNumber, zParent, out _);
            var zListBog = new List<Article_Model>();
            foreach (var rec in zList)
            {
                string Key = rec.Value;
                var Model = new Article_Info(Key).Article;
                zListBog.Add(Model);
            }

            return View("~/Views/Shared/Components/Group2.cshtml", zListBog);
        }
    }
}
