﻿using Microsoft.AspNetCore.Mvc;

namespace WebSite.Component
{
    [ViewComponent]
    public class ProductsRelatedViewComponent : ViewComponent
    {
        public IViewComponentResult Invoke(ParamProductComponent ProductSession)
        {
            var zList = Product_Data.ListShow(
                TN_Helper.PartnerNumber,
                ProductSession.GroupProduct,
                ProductSession.Category, out _);
            ViewBag.Title = ProductSession.GroupName;
            return View("~/Views/Shared/Components/ProductsRelated.cshtml", zList);
        }
    }
}
