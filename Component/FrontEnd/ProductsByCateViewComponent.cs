﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace WebSite.Component
{
    [ViewComponent]
    public class ProductsByCateViewComponent : ViewComponent
    {
        public IViewComponentResult Invoke()
        {
            var ListData = new List<ParamProductGroupComponent>();
            var ParentCate = Product_Category_Data.List(TN_Helper.PartnerNumber, 0, out _);
            foreach (var parent in ParentCate)
            {
                var parentKey = parent.CategoryKey;
                var parentName = parent.CategoryName;

                var childcate = Product_Category_Data.List(TN_Helper.PartnerNumber, parentKey, out _);
                var listProduct = new List<Product_Model>();
                if (childcate.Count > 0)
                {
                    string joined = "";

                    foreach (var c in childcate)
                    {
                        joined += c.CategoryKey + ",";
                    }
                    joined = joined.Remove(joined.LastIndexOf(","), 1);
                    listProduct = Product_Data.ListFront(TN_Helper.PartnerNumber, joined, out _);
                }

                ListData.Add(new ParamProductGroupComponent()
                {
                    GroupName = parentName,
                    GroupData = listProduct,
                });
            }
            return View("~/Views/Shared/Components/ProductsByCate.cshtml", ListData);
        }
    }
}