﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebSite.Component
{
    [ViewComponent]
    public class ProductsQuickViewComponent : ViewComponent
    {
        public IViewComponentResult Invoke(string ProductKey)
        {
            var ListPhoto = Product_Photo_Data.List(ProductKey, out _);
            var zInfo = new Product_Info(ProductKey);
            zInfo.Product.PhotoList = JsonConvert.SerializeObject(ListPhoto);

            return View("~/Views/Shared/Components/ProductsQuickView.cshtml", zInfo.Product);
        }
    }
}
