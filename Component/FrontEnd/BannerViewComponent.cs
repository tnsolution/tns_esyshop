﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebSite.Component
{
    [ViewComponent]
    public class BannerViewComponent: ViewComponent
    {
       
        public IViewComponentResult Invoke()
        {
            var zList = Banner_Data.List(TN_Helper.PartnerNumber, out _);
            zList = zList.Where(s => s.Publish == true).ToList();
            return View("~/Views/Shared/Components/Banner.cshtml", zList);
        }
    }
}
