﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebSite.Component
{
    [ViewComponent]
    public class FooterViewComponent : ViewComponent
    {
        public IViewComponentResult Invoke(string ID)
        {
            string Logo = "";
            var zPartner = new Partner_Info(TN_Helper.PartnerNumber);
            if (zPartner.Partner.Logo_Large.Length > 0)
            {
                Logo = zPartner.Partner.Logo_Large;
            }

            ViewBag.Logo = Logo;

            var zConfig = new Config_Info(ID, TN_Helper.PartnerNumber);
            var zParent = zConfig.Config.ConfigKey;
            var zList = Config_Data.List(TN_Helper.PartnerNumber, zParent, out _);
            var zCOT1 = zList.SingleOrDefault(s => s.ConfigID == "COT1");
            var zCOT2 = zList.Where(s => s.ConfigID == "COT2").ToList();           
            var zCOT3= zList.Where(s => s.ConfigID == "COT3").ToList();
            if (zCOT1 != null)
            {
                string Key = zCOT1.Value;
                var zContact = new Article_Info(Key).Article.Summarize;
                ViewBag.Contact = zContact;
            }

            ViewBag.COT3 = zCOT3;
            ViewBag.COT2 = zCOT2;

            return View("~/Views/Shared/Components/Footer.cshtml");
        }
    }
}
