﻿using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace WebSite.Component
{
    [ViewComponent]
    public class MainMenuViewComponent : ViewComponent
    {
       
        public IViewComponentResult Invoke()
        {
            string Logo = "";
            var zInfo = new Partner_Info(TN_Helper.PartnerNumber);
            if (zInfo.Partner.Logo_Large.Length > 0)
            {
                Logo = zInfo.Partner.Logo_Large;
            }

            ViewBag.Logo = Logo;

            var zList = Menu_WebSite_Data.ListShow(TN_Helper.PartnerNumber, out _);
            zList = zList.Where(s => s.Publish == true).ToList();
            return View("~/Views/Shared/Components/MainMenu.cshtml", zList);
        }
    }
}
