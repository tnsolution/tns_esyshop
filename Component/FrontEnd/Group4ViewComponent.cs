﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebSite.Component.FrontEnd
{
    [ViewComponent]
    public class Group4ViewComponent : ViewComponent
    {
        public IViewComponentResult Invoke(string ID)
        {
            var zInfo = new Config_Info(ID, TN_Helper.PartnerNumber);
            ViewBag.Title = zInfo.Config.Name;

            var zCategory = zInfo.Config.Value;
            var zList = Article_Data.ListShow(TN_Helper.PartnerNumber, zCategory.ToInt(), out _);

            return View("~/Views/Shared/Components/Group4.cshtml", zList);
        }
    }
}
