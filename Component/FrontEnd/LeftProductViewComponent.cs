﻿using Microsoft.AspNetCore.Mvc;

namespace WebSite.Component
{
    [ViewComponent]
    public class LeftProductViewComponent : ViewComponent
    {
        public IViewComponentResult Invoke(int CategoryKey)
        {
            var zCategory = new Product_Category_Info(CategoryKey).Product_Category;
            ViewBag.CategoryName = zCategory.CategoryName;
            ViewBag.CategoryKey = zCategory.CategoryKey;

            var zList = Product_Category_Data.List(TN_Helper.PartnerNumber, CategoryKey, out _);

            if (zList.Count <= 0)
            {
                zList = Product_Category_Data.List(TN_Helper.PartnerNumber, zCategory.Parent, out _);
            }

            return View("~/Views/Shared/Components/LeftProduct.cshtml", zList);
        }
    }
}
