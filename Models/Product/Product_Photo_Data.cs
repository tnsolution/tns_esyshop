﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
namespace WebSite
{
    public class Product_Photo_Data
    {
        public static List<Product_Photo_Model> List(string ProductKey, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM PDT_Product_Photo WHERE RecordStatus != 99 AND ProductKey = @ProductKey ";
            string zConnectionString = TN_Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ProductKey", SqlDbType.NVarChar).Value = ProductKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Product_Photo_Model> zList = new List<Product_Photo_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Product_Photo_Model()
                {
                    PhotoKey = r["PhotoKey"].ToString(),
                    ProductKey = r["ProductKey"].ToString(),
                    PhotoPath = r["PhotoPath"].ToString(),
                    PhotoTitle = r["PhotoTitle"].ToString(),
                    PhotoDescription = r["PhotoDescription"].ToString(),
                    PhotoThumb = r["PhotoThumb"].ToString(),
                    Rank = r["Rank"].ToInt(),
                    Publish = r["Publish"].ToBool(),
                    MainPhoto = r["MainPhoto"].ToBool(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }
    }
}
