﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
namespace WebSite
{
    public class Product_Data
    {
        public static List<Product_Model> List(string PartnerNumber, int CategoryKey, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM PDT_Product WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber AND ProductModel !='' ";
            if (CategoryKey != 0)
                zSQL += " AND CategoryKey IN (@CategoryKey)";
            string zConnectionString = TN_Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.NVarChar).Value = CategoryKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Product_Model> zList = new List<Product_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Product_Model()
                {
                    ProductKey = r["ProductKey"].ToString(),
                    ProductID = r["ProductID"].ToString(),
                    ProductName = r["ProductName"].ToString(),
                    Technical = r["Technical"].ToString(),
                    ProductNumber = r["ProductNumber"].ToString(),
                    ProductSerial = r["ProductSerial"].ToString(),
                    ProductModel = r["ProductModel"].ToString(),
                    StandardCost = r["StandardCost"].ToDouble(),
                    SalePrice = r["SalePrice"].ToDouble(),
                    SaleOff = r["SaleOff"].ToDouble(),
                    TransferRate = r["TransferRate"].ToFloat(),
                    TransferMoney = r["TransferMoney"].ToDouble(),
                    TransferCost = r["TransferCost"].ToDouble(),
                    ProductCost = r["ProductCost"].ToDouble(),
                    VATRate = r["VATRate"].ToFloat(),
                    StandardUnitKey = r["StandardUnitKey"].ToInt(),
                    StandardUnitName = r["StandardUnitName"].ToString(),
                    DiscontinuedDate = (r["DiscontinuedDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["DiscontinuedDate"]),
                    SafetyInStock = r["SafetyInStock"].ToFloat(),
                    PhotoList = r["PhotoList"].ToString(),
                    Style = r["Style"].ToString(),
                    Class = r["Class"].ToString(),
                    ProductLine = r["ProductLine"].ToString(),
                    CategoryKey = r["CategoryKey"].ToInt(),
                    CategoryName = r["CategoryName"].ToString(),
                    CategoryPath = r["CategoryPath"].ToString(),
                    ProductModeKey = r["ProductModeKey"].ToInt(),
                    PhotoPath = r["PhotoPath"].ToString(),
                    StatusKey = r["StatusKey"].ToInt(),
                    StatusName = r["StatusName"].ToString(),
                    BrandKey = r["BrandKey"].ToInt(),
                    BrandName = r["BrandName"].ToString(),
                    Publish = r["Publish"].ToBool(),
                    FrontPage = r["FrontPage"].ToBool(),
                    ProductSummarize = r["ProductSummarize"].ToString(),
                    Description = r["Description"].ToString(),
                    QuantityFact = r["QuantityFact"].ToFloat(),
                    QuantityLog = r["QuantityLog"].ToString(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }

        public static List<Product_Model> ListFront(string PartnerNumber, string CategoryKey, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT TOP 3 * FROM PDT_Product WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber AND FrontPage =1 ";
            if (CategoryKey != string.Empty)
                zSQL += " AND CategoryKey IN ("+ CategoryKey + ")";
            string zConnectionString = TN_Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Product_Model> zList = new List<Product_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Product_Model()
                {
                    ProductKey = r["ProductKey"].ToString(),
                    ProductID = r["ProductID"].ToString(),
                    ProductName = r["ProductName"].ToString(),
                    Technical = r["Technical"].ToString(),
                    ProductNumber = r["ProductNumber"].ToString(),
                    ProductSerial = r["ProductSerial"].ToString(),
                    ProductModel = r["ProductModel"].ToString(),
                    StandardCost = r["StandardCost"].ToDouble(),
                    SalePrice = r["SalePrice"].ToDouble(),
                    SaleOff = r["SaleOff"].ToDouble(),
                    TransferRate = r["TransferRate"].ToFloat(),
                    TransferMoney = r["TransferMoney"].ToDouble(),
                    TransferCost = r["TransferCost"].ToDouble(),
                    ProductCost = r["ProductCost"].ToDouble(),
                    VATRate = r["VATRate"].ToFloat(),
                    StandardUnitKey = r["StandardUnitKey"].ToInt(),
                    StandardUnitName = r["StandardUnitName"].ToString(),
                    DiscontinuedDate = (r["DiscontinuedDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["DiscontinuedDate"]),
                    SafetyInStock = r["SafetyInStock"].ToFloat(),
                    PhotoList = r["PhotoList"].ToString(),
                    Style = r["Style"].ToString(),
                    Class = r["Class"].ToString(),
                    ProductLine = r["ProductLine"].ToString(),
                    CategoryKey = r["CategoryKey"].ToInt(),
                    CategoryName = r["CategoryName"].ToString(),
                    CategoryPath = r["CategoryPath"].ToString(),
                    ProductModeKey = r["ProductModeKey"].ToInt(),
                    PhotoPath = r["PhotoPath"].ToString(),
                    StatusKey = r["StatusKey"].ToInt(),
                    StatusName = r["StatusName"].ToString(),
                    BrandKey = r["BrandKey"].ToInt(),
                    BrandName = r["BrandName"].ToString(),
                    Publish = r["Publish"].ToBool(),
                    FrontPage = r["FrontPage"].ToBool(),
                    ProductSummarize = r["ProductSummarize"].ToString(),
                    Description = r["Description"].ToString(),
                    QuantityFact = r["QuantityFact"].ToFloat(),
                    QuantityLog = r["QuantityLog"].ToString(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }
        public static List<Product_Model> ListShow(string PartnerNumber, string CategoryKey, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM PDT_Product WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber AND Publish = 1  ";
            if (CategoryKey != string.Empty)
                zSQL += " AND CategoryKey IN ("+ CategoryKey + ")";
            string zConnectionString = TN_Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;               
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Product_Model> zList = new List<Product_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Product_Model()
                {
                    ProductKey = r["ProductKey"].ToString(),
                    ProductID = r["ProductID"].ToString(),
                    ProductName = r["ProductName"].ToString(),
                    Technical = r["Technical"].ToString(),
                    ProductNumber = r["ProductNumber"].ToString(),
                    ProductSerial = r["ProductSerial"].ToString(),
                    ProductModel = r["ProductModel"].ToString(),
                    StandardCost = r["StandardCost"].ToDouble(),
                    SalePrice = r["SalePrice"].ToDouble(),
                    SaleOff = r["SaleOff"].ToDouble(),
                    TransferRate = r["TransferRate"].ToFloat(),
                    TransferMoney = r["TransferMoney"].ToDouble(),
                    TransferCost = r["TransferCost"].ToDouble(),
                    ProductCost = r["ProductCost"].ToDouble(),
                    VATRate = r["VATRate"].ToFloat(),
                    StandardUnitKey = r["StandardUnitKey"].ToInt(),
                    StandardUnitName = r["StandardUnitName"].ToString(),
                    DiscontinuedDate = (r["DiscontinuedDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["DiscontinuedDate"]),
                    SafetyInStock = r["SafetyInStock"].ToFloat(),
                    PhotoList = r["PhotoList"].ToString(),
                    Style = r["Style"].ToString(),
                    Class = r["Class"].ToString(),
                    ProductLine = r["ProductLine"].ToString(),
                    CategoryKey = r["CategoryKey"].ToInt(),
                    CategoryName = r["CategoryName"].ToString(),
                    CategoryPath = r["CategoryPath"].ToString(),
                    ProductModeKey = r["ProductModeKey"].ToInt(),
                    PhotoPath = r["PhotoPath"].ToString(),
                    StatusKey = r["StatusKey"].ToInt(),
                    StatusName = r["StatusName"].ToString(),
                    BrandKey = r["BrandKey"].ToInt(),
                    BrandName = r["BrandName"].ToString(),
                    Publish = r["Publish"].ToBool(),
                    FrontPage = r["FrontPage"].ToBool(),
                    ProductSummarize = r["ProductSummarize"].ToString(),
                    Description = r["Description"].ToString(),
                    QuantityFact = r["QuantityFact"].ToFloat(),
                    QuantityLog = r["QuantityLog"].ToString(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }
        public static List<Product_Model> ListShow(string PartnerNumber, string Group, int CategoryKey, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL;
            switch (Group)
            {
                case "NEW":
                    zSQL = @"
SELECT TOP 20
ProductKey, ProductID, ProductName, StandardCost, SalePrice, SaleOff, PhotoPath, PhotoList, CategoryKey, 
CategoryName, CreatedOn, CreatedBy, CreatedName, ModifiedOn, ModifiedBy, ModifiedName
FROM PDT_Product 
WHERE 
RecordStatus != 99 
AND PartnerNumber = @PartnerNumber AND ProductModel !='' AND FrontPage=1 
@AddInParamater
ORDER BY CreatedOn DESC ";
                    break;

                default:
                    zSQL = @"
SELECT TOP 20
ProductKey, ProductID, ProductName, StandardCost, SalePrice, SaleOff, PhotoPath, PhotoList, CategoryKey, 
CategoryName, CreatedOn, CreatedBy, CreatedName, ModifiedOn, ModifiedBy, ModifiedName
FROM PDT_Product 
WHERE 
RecordStatus != 99 
AND PartnerNumber = @PartnerNumber AND ProductModel !='' AND FrontPage=1 
@AddInParamater
ORDER BY CreatedOn DESC ";
                    break;
            }

            if (CategoryKey != 0)
            {
                zSQL = zSQL.Replace("@AddInParamater", " AND CategoryKey = @CategoryKey ");
            }

            zSQL = zSQL.Replace("@AddInParamater", "");
            string zConnectionString = TN_Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = CategoryKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Product_Model> zList = new List<Product_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Product_Model()
                {
                    ProductKey = r["ProductKey"].ToString(),
                    ProductID = r["ProductID"].ToString(),
                    ProductName = r["ProductName"].ToString(),
                    StandardCost = r["StandardCost"].ToDouble(),
                    SalePrice = r["SalePrice"].ToDouble(),
                    SaleOff = r["SaleOff"].ToDouble(),
                    PhotoList = r["PhotoList"].ToString(),
                    CategoryKey = r["CategoryKey"].ToInt(),
                    CategoryName = r["CategoryName"].ToString(),
                    PhotoPath = r["PhotoPath"].ToString(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }
        public static int CheckProductID(string ProductKey, string ProductID, string PartnerNumber, out string Message)
        {

            int zResult = 0;
            string zSQL = @"SELECT COUNT( * ) FROM PDT_Product WHERE RecordStatus != 99 
AND PartnerNumber = @PartnerNumber AND ProductModel !='' 
AND ProductKey != @ProductKey AND ProductID = @ProductID";
            string zConnectionString = TN_Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@ProductKey", SqlDbType.NVarChar).Value = ProductKey;
                zCommand.Parameters.Add("@ProductID", SqlDbType.NVarChar).Value = ProductID;
                zResult = zCommand.ExecuteScalar().ToInt();
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }

            return zResult;
        }
    }
}
