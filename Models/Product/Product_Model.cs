﻿using System;
namespace WebSite
{
    public class Product_Model
    {
        #region [ Field Name ]
        private string _ProductKey = "";
        private string _ProductID = "";
        private string _ProductName = "";
        private string _Technical = "";
        private string _ProductNumber = "";
        private string _ProductSerial = "";
        private string _ProductModel = "";
        private double _StandardCost = 0;
        private double _SalePrice = 0;
        private double _SaleOff = 0;
        private float _TransferRate = 0;
        private double _TransferMoney = 0;
        private double _TransferCost = 0;
        private double _ProductCost = 0;
        private float _VATRate = 0;
        private int _StandardUnitKey = 0;
        private string _StandardUnitName = "";
        private DateTime _DiscontinuedDate = DateTime.MinValue;
        private float _SafetyInStock = 0;
        private string _PhotoList = "";
        private string _Style = "";
        private string _Class = "";
        private string _ProductLine = "";
        private int _CategoryKey = 0;
        private string _CategoryName = "";
        private string _CategoryPath = "";
        private int _ProductModeKey = 0;
        private string _PhotoPath = "";
        private int _StatusKey = 0;
        private string _StatusName = "";
        private int _BrandKey = 0;
        private string _BrandName = "";
        private bool _Publish = false;
        private bool _FrontPage = false;
        private string _ProductSummarize = "";
        private string _Description = "";
        private float _QuantityFact = 0;
        private string _QuantityLog = "";
        private string _PartnerNumber = "";
        private int _RecordStatus = 0;
        private DateTime _CreatedOn = DateTime.MinValue;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedOn = DateTime.MinValue;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        #endregion

        #region [ Properties ]
        public string ProductKey
        {
            get { return _ProductKey; }
            set { _ProductKey = value; }
        }
        public string ProductID
        {
            get { return _ProductID; }
            set { _ProductID = value; }
        }
        public string ProductName
        {
            get { return _ProductName; }
            set { _ProductName = value; }
        }
        public string Technical
        {
            get { return _Technical; }
            set { _Technical = value; }
        }
        public string ProductNumber
        {
            get { return _ProductNumber; }
            set { _ProductNumber = value; }
        }
        public string ProductSerial
        {
            get { return _ProductSerial; }
            set { _ProductSerial = value; }
        }
        public string ProductModel
        {
            get { return _ProductModel; }
            set { _ProductModel = value; }
        }
        public double StandardCost
        {
            get { return _StandardCost; }
            set { _StandardCost = value; }
        }
        public double SalePrice
        {
            get { return _SalePrice; }
            set { _SalePrice = value; }
        }
        public double SaleOff
        {
            get { return _SaleOff; }
            set { _SaleOff = value; }
        }
        public float TransferRate
        {
            get { return _TransferRate; }
            set { _TransferRate = value; }
        }
        public double TransferMoney
        {
            get { return _TransferMoney; }
            set { _TransferMoney = value; }
        }
        public double TransferCost
        {
            get { return _TransferCost; }
            set { _TransferCost = value; }
        }
        public double ProductCost
        {
            get { return _ProductCost; }
            set { _ProductCost = value; }
        }
        public float VATRate
        {
            get { return _VATRate; }
            set { _VATRate = value; }
        }
        public int StandardUnitKey
        {
            get { return _StandardUnitKey; }
            set { _StandardUnitKey = value; }
        }
        public string StandardUnitName
        {
            get { return _StandardUnitName; }
            set { _StandardUnitName = value; }
        }
        public DateTime DiscontinuedDate
        {
            get { return _DiscontinuedDate; }
            set { _DiscontinuedDate = value; }
        }
        public float SafetyInStock
        {
            get { return _SafetyInStock; }
            set { _SafetyInStock = value; }
        }
        public string PhotoList
        {
            get { return _PhotoList; }
            set { _PhotoList = value; }
        }
        public string Style
        {
            get { return _Style; }
            set { _Style = value; }
        }
        public string Class
        {
            get { return _Class; }
            set { _Class = value; }
        }
        public string ProductLine
        {
            get { return _ProductLine; }
            set { _ProductLine = value; }
        }
        public int CategoryKey
        {
            get { return _CategoryKey; }
            set { _CategoryKey = value; }
        }
        public string CategoryName
        {
            get { return _CategoryName; }
            set { _CategoryName = value; }
        }
        public string CategoryPath
        {
            get { return _CategoryPath; }
            set { _CategoryPath = value; }
        }
        public int ProductModeKey
        {
            get { return _ProductModeKey; }
            set { _ProductModeKey = value; }
        }
        public string PhotoPath
        {
            get { return _PhotoPath; }
            set { _PhotoPath = value; }
        }
        public int StatusKey
        {
            get { return _StatusKey; }
            set { _StatusKey = value; }
        }
        public string StatusName
        {
            get { return _StatusName; }
            set { _StatusName = value; }
        }
        public int BrandKey
        {
            get { return _BrandKey; }
            set { _BrandKey = value; }
        }
        public string BrandName
        {
            get { return _BrandName; }
            set { _BrandName = value; }
        }
        public bool Publish
        {
            get { return _Publish; }
            set { _Publish = value; }
        }
        public bool FrontPage
        {
            get { return _FrontPage; }
            set { _FrontPage = value; }
        }
        public string ProductSummarize
        {
            get { return _ProductSummarize; }
            set { _ProductSummarize = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public float QuantityFact
        {
            get { return _QuantityFact; }
            set { _QuantityFact = value; }
        }
        public string QuantityLog
        {
            get { return _QuantityLog; }
            set { _QuantityLog = value; }
        }
        public string PartnerNumber
        {
            get { return _PartnerNumber; }
            set { _PartnerNumber = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        #endregion
    }
}
