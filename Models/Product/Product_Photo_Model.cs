﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace WebSite
{
public class Product_Photo_Model
{
#region [ Field Name ]
private string _PhotoKey = "";
private string _ProductKey = "";
private string _PhotoPath = "";
private string _PhotoTitle = "";
private string _PhotoDescription = "";
private string _PhotoThumb = "";
private int _Rank = 0;
private bool _Publish;
private bool _MainPhoto;
private int _RecordStatus = 0;
private string _PartnerNumber = "";
private DateTime _CreatedOn = DateTime.MinValue;
private string _CreatedBy = "";
private string _CreatedName = "";
private DateTime _ModifiedOn = DateTime.MinValue;
private string _ModifiedBy = "";
private string _ModifiedName = "";
#endregion
 
#region [ Properties ]
public string PhotoKey
{
get { return _PhotoKey; }
set { _PhotoKey = value; }
}
public string ProductKey
{
get { return _ProductKey; }
set { _ProductKey = value; }
}
public string PhotoPath
{
get { return _PhotoPath; }
set { _PhotoPath = value; }
}
public string PhotoTitle
{
get { return _PhotoTitle; }
set { _PhotoTitle = value; }
}
public string PhotoDescription
{
get { return _PhotoDescription; }
set { _PhotoDescription = value; }
}
public string PhotoThumb
{
get { return _PhotoThumb; }
set { _PhotoThumb = value; }
}
public int Rank
{
get { return _Rank; }
set { _Rank = value; }
}
public bool Publish
{
get { return _Publish; }
set { _Publish = value; }
}
public bool MainPhoto
{
get { return _MainPhoto; }
set { _MainPhoto = value; }
}
public int RecordStatus
{
get { return _RecordStatus; }
set { _RecordStatus = value; }
}
public string PartnerNumber
{
get { return _PartnerNumber; }
set { _PartnerNumber = value; }
}
public DateTime CreatedOn
{
get { return _CreatedOn; }
set { _CreatedOn = value; }
}
public string CreatedBy
{
get { return _CreatedBy; }
set { _CreatedBy = value; }
}
public string CreatedName
{
get { return _CreatedName; }
set { _CreatedName = value; }
}
public DateTime ModifiedOn
{
get { return _ModifiedOn; }
set { _ModifiedOn = value; }
}
public string ModifiedBy
{
get { return _ModifiedBy; }
set { _ModifiedBy = value; }
}
public string ModifiedName
{
get { return _ModifiedName; }
set { _ModifiedName = value; }
}
#endregion
}
}
