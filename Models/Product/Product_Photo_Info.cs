﻿using System;
using System.Data;
using System.Data.SqlClient;
namespace WebSite
{
    public class Product_Photo_Info
    {

        public Product_Photo_Model Product_Photo = new Product_Photo_Model();
        private string _Message = "";
        public string Code
        {
            get
            {
                if (_Message.Length >= 3)
                    return _Message.Substring(0, 3);
                else return "";
            }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        #region [ Constructor Get Information ]
        public Product_Photo_Info()
        {
            Product_Photo.PhotoKey = Guid.NewGuid().ToString();
        }
        public Product_Photo_Info(string PhotoKey)
        {
            string zSQL = "SELECT * FROM PDT_Product_Photo WHERE PhotoKey = @PhotoKey AND RecordStatus != 99 ";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@PhotoKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(PhotoKey);
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    Product_Photo.PhotoKey = zReader["PhotoKey"].ToString();
                    Product_Photo.ProductKey = zReader["ProductKey"].ToString();
                    Product_Photo.PhotoPath = zReader["PhotoPath"].ToString();
                    Product_Photo.PhotoTitle = zReader["PhotoTitle"].ToString();
                    Product_Photo.PhotoDescription = zReader["PhotoDescription"].ToString();
                    Product_Photo.PhotoThumb = zReader["PhotoThumb"].ToString();
                    if (zReader["Rank"] != DBNull.Value)
                        Product_Photo.Rank = int.Parse(zReader["Rank"].ToString());
                    if (zReader["Publish"] != DBNull.Value)
                        Product_Photo.Publish = (bool)zReader["Publish"];
                    if (zReader["MainPhoto"] != DBNull.Value)
                        Product_Photo.MainPhoto = (bool)zReader["MainPhoto"];
                    if (zReader["RecordStatus"] != DBNull.Value)
                        Product_Photo.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    Product_Photo.PartnerNumber = zReader["PartnerNumber"].ToString();
                    if (zReader["CreatedOn"] != DBNull.Value)
                        Product_Photo.CreatedOn = (DateTime)zReader["CreatedOn"];
                    Product_Photo.CreatedBy = zReader["CreatedBy"].ToString();
                    Product_Photo.CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        Product_Photo.ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    Product_Photo.ModifiedBy = zReader["ModifiedBy"].ToString();
                    Product_Photo.ModifiedName = zReader["ModifiedName"].ToString();
                    _Message = "200 OK";
                }
                else
                {
                    _Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }

        #endregion

        #region [ Constructor Update Information ]

        public string Create_ServerKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO PDT_Product_Photo ("
         + " ProductKey , PhotoPath , PhotoTitle , PhotoDescription , PhotoThumb , Rank , Publish , MainPhoto , RecordStatus , PartnerNumber , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @ProductKey , @PhotoPath , @PhotoTitle , @PhotoDescription , @PhotoThumb , @Rank , @Publish , @MainPhoto , @RecordStatus , @PartnerNumber , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (Product_Photo.ProductKey != "" && Product_Photo.ProductKey.Length == 36)
                {
                    zCommand.Parameters.Add("@ProductKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Product_Photo.ProductKey);
                }
                else
                    zCommand.Parameters.Add("@ProductKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@PhotoPath", SqlDbType.NVarChar).Value = Product_Photo.PhotoPath;
                zCommand.Parameters.Add("@PhotoTitle", SqlDbType.NVarChar).Value = Product_Photo.PhotoTitle;
                zCommand.Parameters.Add("@PhotoDescription", SqlDbType.NVarChar).Value = Product_Photo.PhotoDescription;
                zCommand.Parameters.Add("@PhotoThumb", SqlDbType.NVarChar).Value = Product_Photo.PhotoThumb;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = Product_Photo.Rank;

                zCommand.Parameters.Add("@Publish", SqlDbType.Bit).Value = Product_Photo.Publish;

                zCommand.Parameters.Add("@MainPhoto", SqlDbType.Bit).Value = Product_Photo.MainPhoto;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Product_Photo.RecordStatus;
                if (Product_Photo.PartnerNumber != "" && Product_Photo.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Product_Photo.PartnerNumber);
                }
                else
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Product_Photo.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Product_Photo.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Product_Photo.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Product_Photo.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Create_ClientKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO PDT_Product_Photo("
         + " PhotoKey , ProductKey , PhotoPath , PhotoTitle , PhotoDescription , PhotoThumb , Rank , Publish , MainPhoto , RecordStatus , PartnerNumber , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @PhotoKey , @ProductKey , @PhotoPath , @PhotoTitle , @PhotoDescription , @PhotoThumb , @Rank , @Publish , @MainPhoto , @RecordStatus , @PartnerNumber , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (Product_Photo.PhotoKey != "" && Product_Photo.PhotoKey.Length == 36)
                {
                    zCommand.Parameters.Add("@PhotoKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Product_Photo.PhotoKey);
                }
                else
                    zCommand.Parameters.Add("@PhotoKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                if (Product_Photo.ProductKey != "" && Product_Photo.ProductKey.Length == 36)
                {
                    zCommand.Parameters.Add("@ProductKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Product_Photo.ProductKey);
                }
                else
                    zCommand.Parameters.Add("@ProductKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@PhotoPath", SqlDbType.NVarChar).Value = Product_Photo.PhotoPath;
                zCommand.Parameters.Add("@PhotoTitle", SqlDbType.NVarChar).Value = Product_Photo.PhotoTitle;
                zCommand.Parameters.Add("@PhotoDescription", SqlDbType.NVarChar).Value = Product_Photo.PhotoDescription;
                zCommand.Parameters.Add("@PhotoThumb", SqlDbType.NVarChar).Value = Product_Photo.PhotoThumb;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = Product_Photo.Rank;

                zCommand.Parameters.Add("@Publish", SqlDbType.Bit).Value = Product_Photo.Publish;

                zCommand.Parameters.Add("@MainPhoto", SqlDbType.Bit).Value = Product_Photo.MainPhoto;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Product_Photo.RecordStatus;
                if (Product_Photo.PartnerNumber != "" && Product_Photo.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Product_Photo.PartnerNumber);
                }
                else
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Product_Photo.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Product_Photo.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Product_Photo.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Product_Photo.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Update()
        {
            string zSQL = "UPDATE PDT_Product_Photo SET "
                        + " ProductKey = @ProductKey,"
                        + " PhotoPath = @PhotoPath,"
                        + " PhotoTitle = @PhotoTitle,"
                        + " PhotoDescription = @PhotoDescription,"
                        + " PhotoThumb = @PhotoThumb,"
                        + " Rank = @Rank,"
                        + " Publish = @Publish,"
                        + " MainPhoto = @MainPhoto,"
                        + " RecordStatus = @RecordStatus,"
                        + " PartnerNumber = @PartnerNumber,"
                        + " ModifiedOn = GetDate(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                        + " WHERE PhotoKey = @PhotoKey";
            string zResult = "";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (Product_Photo.PhotoKey != "" && Product_Photo.PhotoKey.Length == 36)
                {
                    zCommand.Parameters.Add("@PhotoKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Product_Photo.PhotoKey);
                }
                else
                    zCommand.Parameters.Add("@PhotoKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                if (Product_Photo.ProductKey != "" && Product_Photo.ProductKey.Length == 36)
                {
                    zCommand.Parameters.Add("@ProductKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Product_Photo.ProductKey);
                }
                else
                    zCommand.Parameters.Add("@ProductKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@PhotoPath", SqlDbType.NVarChar).Value = Product_Photo.PhotoPath;
                zCommand.Parameters.Add("@PhotoTitle", SqlDbType.NVarChar).Value = Product_Photo.PhotoTitle;
                zCommand.Parameters.Add("@PhotoDescription", SqlDbType.NVarChar).Value = Product_Photo.PhotoDescription;
                zCommand.Parameters.Add("@PhotoThumb", SqlDbType.NVarChar).Value = Product_Photo.PhotoThumb;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = Product_Photo.Rank;

                zCommand.Parameters.Add("@Publish", SqlDbType.Bit).Value = Product_Photo.Publish;

                zCommand.Parameters.Add("@MainPhoto", SqlDbType.Bit).Value = Product_Photo.MainPhoto;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Product_Photo.RecordStatus;
                if (Product_Photo.PartnerNumber != "" && Product_Photo.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Product_Photo.PartnerNumber);
                }
                else
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Product_Photo.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Product_Photo.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE PDT_Product_Photo SET RecordStatus = 99 WHERE PhotoKey = @PhotoKey";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PhotoKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Product_Photo.PhotoKey);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Empty()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM PDT_Product_Photo WHERE PhotoKey = @PhotoKey";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PhotoKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Product_Photo.PhotoKey);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
