﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebSite
{
    public class ParamProductGroupComponent
    {
        public string GroupName { get; set; } = "";
        public List<Product_Model> GroupData { get; set; } = new List<Product_Model>();
    }
}
