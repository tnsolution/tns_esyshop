﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebSite
{
    public class ParamMenuAdminComponent
    {
        public List<Article_Category_Model> ListCategory { get; set; } = new List<Article_Category_Model>();
        public List<Product_Category_Model> ListProduct { get; set; } = new List<Product_Category_Model>();
    }
}
