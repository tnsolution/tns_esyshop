﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebSite
{
    /// <summary>
    /// Các tham số dùng để xác định nhóm sản phẩm cần hiển thị
    /// </summary>
    public class ParamProductComponent
    {
        /// <summary>
        /// Tên title hiển thị ra view
        /// </summary>
        public string GroupName { get; set; } = "";
        /// <summary>
        /// Nhóm sản phẩm eg NEW, HOT, ....
        /// </summary>
        public string GroupProduct { get; set; } = "";
        /// <summary>
        /// Category của dòng sản phẩm cùng loại
        /// </summary>
        public int Category { get; set; } = 0;
    }
}
