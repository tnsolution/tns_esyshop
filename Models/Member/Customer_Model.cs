﻿using System;
namespace WebSite
{
    public class Customer_Model
    {
        #region [ Field Name ]
        private string _CustomerKey = "";
        private string _CustomerID = "";
        private string _FullName = "";
        private string _CompanyName = "";
        private string _Aliases = "";
        private string _JobTitle = "";
        private int _CategoryKey = 0;
        private string _CategoryName = "";
        private int _CustomerType = 0;
        private string _CustomerTypeName = "";
        private int _CustomerVendor = 0;
        private string _TaxNumber = "";
        private string _IDCard = "";
        private string _Address = "";
        private string _City = "";
        private string _Country = "";
        private string _ZipCode = "";
        private string _Phone = "";
        private string _Email = "";
        private string _Note = "";
        private string _Style = "";
        private string _Class = "";
        private string _CodeLine = "";
        private string _BankAccount = "";
        private string _BankName = "";
        private string _Referrer = "";
        private string _Password = "";
        private string _Parent = "";
        private int _Slug = 0;
        private string _PartnerNumber = "";
        private string _Contact = "";
        private string _OrganizationID = "";
        private string _DepartmentKey = "";
        private string _BranchKey = "";
        private int _RecordStatus = 0;
        private DateTime _CreatedOn = DateTime.MinValue;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedOn = DateTime.MinValue;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private DateTime _LastLoginDate = DateTime.MinValue;
        #endregion

        #region [ Properties ]
        public string CustomerKey
        {
            get { return _CustomerKey; }
            set { _CustomerKey = value; }
        }
        public string CustomerID
        {
            get { return _CustomerID; }
            set { _CustomerID = value; }
        }
        public string FullName
        {
            get { return _FullName; }
            set { _FullName = value; }
        }
        public string CompanyName
        {
            get { return _CompanyName; }
            set { _CompanyName = value; }
        }
        public string Aliases
        {
            get { return _Aliases; }
            set { _Aliases = value; }
        }
        public string JobTitle
        {
            get { return _JobTitle; }
            set { _JobTitle = value; }
        }
        public int CategoryKey
        {
            get { return _CategoryKey; }
            set { _CategoryKey = value; }
        }
        public string CategoryName
        {
            get { return _CategoryName; }
            set { _CategoryName = value; }
        }
        public int CustomerType
        {
            get { return _CustomerType; }
            set { _CustomerType = value; }
        }
        public string CustomerTypeName
        {
            get { return _CustomerTypeName; }
            set { _CustomerTypeName = value; }
        }
        public int CustomerVendor
        {
            get { return _CustomerVendor; }
            set { _CustomerVendor = value; }
        }
        public string TaxNumber
        {
            get { return _TaxNumber; }
            set { _TaxNumber = value; }
        }
        public string IDCard
        {
            get { return _IDCard; }
            set { _IDCard = value; }
        }
        public string Address
        {
            get { return _Address; }
            set { _Address = value; }
        }
        public string City
        {
            get { return _City; }
            set { _City = value; }
        }
        public string Country
        {
            get { return _Country; }
            set { _Country = value; }
        }
        public string ZipCode
        {
            get { return _ZipCode; }
            set { _ZipCode = value; }
        }
        public string Phone
        {
            get { return _Phone; }
            set { _Phone = value; }
        }
        public string Email
        {
            get { return _Email; }
            set { _Email = value; }
        }
        public string Note
        {
            get { return _Note; }
            set { _Note = value; }
        }
        public string Style
        {
            get { return _Style; }
            set { _Style = value; }
        }
        public string Class
        {
            get { return _Class; }
            set { _Class = value; }
        }
        public string CodeLine
        {
            get { return _CodeLine; }
            set { _CodeLine = value; }
        }
        public string BankAccount
        {
            get { return _BankAccount; }
            set { _BankAccount = value; }
        }
        public string BankName
        {
            get { return _BankName; }
            set { _BankName = value; }
        }
        public string Referrer
        {
            get { return _Referrer; }
            set { _Referrer = value; }
        }
        public string Password
        {
            get { return _Password; }
            set { _Password = value; }
        }
        public string Parent
        {
            get { return _Parent; }
            set { _Parent = value; }
        }
        public int Slug
        {
            get { return _Slug; }
            set { _Slug = value; }
        }
        public string PartnerNumber
        {
            get { return _PartnerNumber; }
            set { _PartnerNumber = value; }
        }
        public string Contact
        {
            get { return _Contact; }
            set { _Contact = value; }
        }
        public string OrganizationID
        {
            get { return _OrganizationID; }
            set { _OrganizationID = value; }
        }
        public string DepartmentKey
        {
            get { return _DepartmentKey; }
            set { _DepartmentKey = value; }
        }
        public string BranchKey
        {
            get { return _BranchKey; }
            set { _BranchKey = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public DateTime LastLoginDate
        {
            get { return _LastLoginDate; }
            set { _LastLoginDate = value; }
        }
        #endregion
    }
}
