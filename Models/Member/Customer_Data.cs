﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
namespace WebSite
{
    public class Customer_Data
    {
        public static List<Customer_Model> List(string PartnerNumber, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM CRM_Customer WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ";
            string zConnectionString = TN_Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Customer_Model> zList = new List<Customer_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Customer_Model()
                {
                    CustomerKey = r["CustomerKey"].ToString(),
                    CustomerID = r["CustomerID"].ToString(),
                    FullName = r["FullName"].ToString(),
                    CompanyName = r["CompanyName"].ToString(),
                    Aliases = r["Aliases"].ToString(),
                    JobTitle = r["JobTitle"].ToString(),
                    CategoryKey = r["CategoryKey"].ToInt(),
                    CategoryName = r["CategoryName"].ToString(),
                    CustomerType = r["CustomerType"].ToInt(),
                    CustomerTypeName = r["CustomerTypeName"].ToString(),
                    CustomerVendor = r["CustomerVendor"].ToInt(),
                    TaxNumber = r["TaxNumber"].ToString(),
                    IDCard = r["IDCard"].ToString(),
                    Address = r["Address"].ToString(),
                    City = r["City"].ToString(),
                    Country = r["Country"].ToString(),
                    ZipCode = r["ZipCode"].ToString(),
                    Phone = r["Phone"].ToString(),
                    Email = r["Email"].ToString(),
                    Note = r["Note"].ToString(),
                    Style = r["Style"].ToString(),
                    Class = r["Class"].ToString(),
                    CodeLine = r["CodeLine"].ToString(),
                    BankAccount = r["BankAccount"].ToString(),
                    BankName = r["BankName"].ToString(),
                    Referrer = r["Referrer"].ToString(),
                    Password = r["Password"].ToString(),
                    Parent = r["Parent"].ToString(),
                    Slug = r["Slug"].ToInt(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    Contact = r["Contact"].ToString(),
                    OrganizationID = r["OrganizationID"].ToString(),
                    DepartmentKey = r["DepartmentKey"].ToString(),
                    BranchKey = r["BranchKey"].ToString(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                    LastLoginDate = (r["LastLoginDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["LastLoginDate"]),
                });
            }
            return zList;
        }
    }
}
