﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace WebSite
{
    public class Article_Info
    {

        public Article_Model Article = new Article_Model();
        private string _Message = "";
        public string Code
        {
            get
            {
                if (_Message.Length >= 3)
                {
                    return _Message.Substring(0, 3);
                }
                else
                {
                    return "";
                }
            }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        #region [ Constructor Get Information ]
        public Article_Info()
        {
            Article.ArticleKey = Guid.NewGuid().ToString();
        }
        public Article_Info(string ArticleKey)
        {
            string zSQL = "SELECT * FROM PDT_Article WHERE ArticleKey = @ArticleKey AND RecordStatus != 99 ";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ArticleKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(ArticleKey);
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    Article.ArticleKey = zReader["ArticleKey"].ToString();
                    if (zReader["ArticleStatus"] != DBNull.Value)
                    {
                        Article.ArticleStatus = int.Parse(zReader["ArticleStatus"].ToString());
                    }
                    Article.ArticleTag = zReader["ArticleTag"].ToString();
                    Article.ArticleID = zReader["ArticleID"].ToString();
                    Article.ArticleName = zReader["ArticleName"].ToString();
                    Article.Summarize = zReader["Summarize"].ToString();
                    Article.ArticleContent = zReader["ArticleContent"].ToString();
                    if (zReader["CategoryKey"] != DBNull.Value)
                    {
                        Article.CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                    }

                    Article.CategoryName = zReader["CategoryName"].ToString();
                    Article.ImageSmall = zReader["ImageSmall"].ToString();
                    Article.ImageLarge = zReader["ImageLarge"].ToString();
                    Article.FileAttack = zReader["FileAttack"].ToString();
                    if (zReader["Publish"] != DBNull.Value)
                    {
                        Article.Publish = (bool)zReader["Publish"];
                    }

                    if (zReader["Hits"] != DBNull.Value)
                    {
                        Article.Hits = int.Parse(zReader["Hits"].ToString());
                    }

                    if (zReader["Rank"] != DBNull.Value)
                    {
                        Article.Rank = int.Parse(zReader["Rank"].ToString());
                    }

                    if (zReader["Activate"] != DBNull.Value)
                    {
                        Article.Activate = (bool)zReader["Activate"];
                    }

                    Article.PartnerNumber = zReader["PartnerNumber"].ToString();
                    if (zReader["RecordStatus"] != DBNull.Value)
                    {
                        Article.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    }

                    if (zReader["CreatedOn"] != DBNull.Value)
                    {
                        Article.CreatedOn = (DateTime)zReader["CreatedOn"];
                    }

                    Article.CreatedBy = zReader["CreatedBy"].ToString();
                    Article.CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                    {
                        Article.ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    }

                    Article.ModifiedBy = zReader["ModifiedBy"].ToString();
                    Article.ModifiedName = zReader["ModifiedName"].ToString();
                    _Message = "200 OK";
                }
                else
                {
                    _Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }

        #endregion
        #region [ Constructor Update Information ]
        public string Create_ServerKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO PDT_Article ("
         + " ArticleStatus , ArticleID , ArticleTag , MetaDescription , MetaKeyWord , ArticleName , Summarize , ArticleContent , CategoryKey , CategoryName , ImageSmall , ImageLarge , FileAttack , FileName , Publish , Hits , Rank , Activate , Slug , PartnerNumber , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @ArticleStatus , @ArticleID , @ArticleTag , @MetaDescription , @MetaKeyWord , @ArticleName , @Summarize , @ArticleContent , @CategoryKey , @CategoryName , @ImageSmall , @ImageLarge , @FileAttack , @FileName , @Publish , @Hits , @Rank , @Activate , @Slug , @PartnerNumber , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ArticleStatus", SqlDbType.Int).Value = Article.ArticleStatus;
                zCommand.Parameters.Add("@ArticleID", SqlDbType.NVarChar).Value = Article.ArticleID;
                zCommand.Parameters.Add("@ArticleTag", SqlDbType.NVarChar).Value = Article.ArticleTag;
                zCommand.Parameters.Add("@MetaDescription", SqlDbType.NVarChar).Value = Article.MetaDescription;
                zCommand.Parameters.Add("@MetaKeyWord", SqlDbType.NVarChar).Value = Article.MetaKeyWord;
                zCommand.Parameters.Add("@ArticleName", SqlDbType.NVarChar).Value = Article.ArticleName;
                zCommand.Parameters.Add("@Summarize", SqlDbType.NVarChar).Value = Article.Summarize;
                zCommand.Parameters.Add("@ArticleContent", SqlDbType.NVarChar).Value = Article.ArticleContent;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = Article.CategoryKey;
                zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = Article.CategoryName;
                zCommand.Parameters.Add("@ImageSmall", SqlDbType.NVarChar).Value = Article.ImageSmall;
                zCommand.Parameters.Add("@ImageLarge", SqlDbType.NVarChar).Value = Article.ImageLarge;
                zCommand.Parameters.Add("@FileAttack", SqlDbType.NVarChar).Value = Article.FileAttack;
                zCommand.Parameters.Add("@FileName", SqlDbType.NVarChar).Value = Article.FileName;
                zCommand.Parameters.Add("@Publish", SqlDbType.Bit).Value = Article.Publish;
                zCommand.Parameters.Add("@Hits", SqlDbType.Int).Value = Article.Hits;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = Article.Rank;
                zCommand.Parameters.Add("@Activate", SqlDbType.Bit).Value = Article.Activate;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = Article.Slug;
                if (Article.PartnerNumber != "" && Article.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Article.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Article.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Article.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Article.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Article.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Article.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Create_ClientKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO PDT_Article("
         + " ArticleTag, ArticleKey , ArticleStatus , ArticleID , ArticleName , Summarize , ArticleContent , CategoryKey , CategoryName , ImageSmall , ImageLarge , FileAttack , Publish , Hits , Rank , Activate , PartnerNumber , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @ArticleTag, @ArticleKey , @ArticleStatus , @ArticleID , @ArticleName , @Summarize , @ArticleContent , @CategoryKey , @CategoryName , @ImageSmall , @ImageLarge , @FileAttack , @Publish , @Hits , @Rank , @Activate , @PartnerNumber , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ArticleTag", SqlDbType.NVarChar).Value = Article.ArticleTag;
                if (Article.ArticleKey != "" && Article.ArticleKey.Length == 36)
                {
                    zCommand.Parameters.Add("@ArticleKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Article.ArticleKey);
                }
                else
                {
                    zCommand.Parameters.Add("@ArticleKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@ArticleStatus", SqlDbType.Int).Value = Article.ArticleStatus;
                zCommand.Parameters.Add("@ArticleID", SqlDbType.NVarChar).Value = Article.ArticleID;
                zCommand.Parameters.Add("@ArticleName", SqlDbType.NVarChar).Value = Article.ArticleName;
                zCommand.Parameters.Add("@Summarize", SqlDbType.NVarChar).Value = Article.Summarize;
                zCommand.Parameters.Add("@ArticleContent", SqlDbType.NVarChar).Value = Article.ArticleContent;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = Article.CategoryKey;
                zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = Article.CategoryName;
                zCommand.Parameters.Add("@ImageSmall", SqlDbType.NVarChar).Value = Article.ImageSmall;
                zCommand.Parameters.Add("@ImageLarge", SqlDbType.NVarChar).Value = Article.ImageLarge;
                zCommand.Parameters.Add("@FileAttack", SqlDbType.NVarChar).Value = Article.FileAttack;
                zCommand.Parameters.Add("@Publish", SqlDbType.Bit).Value = Article.Publish;
                zCommand.Parameters.Add("@Hits", SqlDbType.Int).Value = Article.Hits;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = Article.Rank;
                zCommand.Parameters.Add("@Activate", SqlDbType.Bit).Value = Article.Activate;
                if (Article.PartnerNumber != "" && Article.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Article.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Article.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Article.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Article.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Article.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Article.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE PDT_Article SET "
                        + " ArticleStatus = @ArticleStatus, ArticleTag = @ArticleTag,"
                        + " ArticleID = @ArticleID,"
                        + " ArticleName = @ArticleName,"
                        + " Summarize = @Summarize,"
                        + " ArticleContent = @ArticleContent,"
                        + " CategoryKey = @CategoryKey,"
                        + " CategoryName = @CategoryName,"
                        + " ImageSmall = @ImageSmall,"
                        + " ImageLarge = @ImageLarge,"
                        + " FileAttack = @FileAttack,"
                        + " Publish = @Publish,"
                        + " Hits = @Hits,"
                        + " Rank = @Rank,"
                        + " Activate = @Activate,"
                        + " PartnerNumber = @PartnerNumber,"
                        + " RecordStatus = @RecordStatus,"
                        + " ModifiedOn = GetDate(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                        + " WHERE ArticleKey = @ArticleKey";
            string zResult = "";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ArticleTag", SqlDbType.NVarChar).Value = Article.ArticleTag;
                if (Article.ArticleKey != "" && Article.ArticleKey.Length == 36)
                {
                    zCommand.Parameters.Add("@ArticleKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Article.ArticleKey);
                }
                else
                {
                    zCommand.Parameters.Add("@ArticleKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@ArticleStatus", SqlDbType.Int).Value = Article.ArticleStatus;
                zCommand.Parameters.Add("@ArticleID", SqlDbType.NVarChar).Value = Article.ArticleID;
                zCommand.Parameters.Add("@ArticleName", SqlDbType.NVarChar).Value = Article.ArticleName;
                zCommand.Parameters.Add("@Summarize", SqlDbType.NVarChar).Value = Article.Summarize;
                zCommand.Parameters.Add("@ArticleContent", SqlDbType.NVarChar).Value = Article.ArticleContent;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = Article.CategoryKey;
                zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = Article.CategoryName;
                zCommand.Parameters.Add("@ImageSmall", SqlDbType.NVarChar).Value = Article.ImageSmall;
                zCommand.Parameters.Add("@ImageLarge", SqlDbType.NVarChar).Value = Article.ImageLarge;
                zCommand.Parameters.Add("@FileAttack", SqlDbType.NVarChar).Value = Article.FileAttack;
                zCommand.Parameters.Add("@Publish", SqlDbType.Bit).Value = Article.Publish;
                zCommand.Parameters.Add("@Hits", SqlDbType.Int).Value = Article.Hits;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = Article.Rank;
                zCommand.Parameters.Add("@Activate", SqlDbType.Bit).Value = Article.Activate;
                if (Article.PartnerNumber != "" && Article.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Article.PartnerNumber);
                }
                else
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Article.RecordStatus;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Article.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Article.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE PDT_Article SET RecordStatus = 99 WHERE ArticleKey = @ArticleKey";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ArticleKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Article.ArticleKey);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Empty()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM PDT_Article WHERE ArticleKey = @ArticleKey";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ArticleKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Article.ArticleKey);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string SetActivate(string ArticleKey)
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = @"UPDATE PDT_Article SET Activate = (CASE Activate WHEN 'true' THEN 'false' ELSE 'true' END),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName,"
                        + " ModifiedOn = GetDate()"
                        + " WHERE ArticleKey = @ArticleKey";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ArticleKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(ArticleKey);
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Article.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Article.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                Message = "200 OK";
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close(); ;
            }
            return zResult;
        }
        public string SetPublish(string ArticleKey)
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = @"UPDATE PDT_Article SET Publish = (CASE Publish WHEN 'true' THEN 'false' ELSE 'true' END),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName,"
                        + " ModifiedOn = GetDate()"
                        + " WHERE ArticleKey = @ArticleKey";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ArticleKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(ArticleKey);
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Article.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Article.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                Message = "200 OK";
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close(); ;
            }
            return zResult;
        }
        #endregion
    }
}
