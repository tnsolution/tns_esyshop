﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace WebSite
{
    public class Article_Data
    {
        /// <summary>
        /// Danh sách dùng để select key bài viết cho trang chủ
        /// </summary>
        /// <param name="PartnerNumber"></param>
        /// <param name="Message"></param>
        /// <returns></returns>
        public static List<Article_Model> ListForSelect(string PartnerNumber, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT ArticleKey, ArticleID, ArticleName FROM PDT_Article WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber";

            zSQL += " ORDER By CreatedOn DESC";
            string zConnectionString = TN_Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
                Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Article_Model> zList = new List<Article_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Article_Model()
                {
                    ArticleKey = r["ArticleKey"].ToString(),                   
                    ArticleID = r["ArticleID"].ToString(),
                    ArticleName = r["ArticleName"].ToString(),
                });
            }
            return zList;
        }
        public static List<Article_Model> List(string PartnerNumber, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM PDT_Article WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber AND Slug = 0";

            zSQL += " ORDER By CreatedOn DESC";
            string zConnectionString = TN_Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
                Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Article_Model> zList = new List<Article_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Article_Model()
                {
                    ArticleKey = r["ArticleKey"].ToString(),
                    ArticleStatus = r["ArticleStatus"].ToInt(),
                    ArticleID = r["ArticleID"].ToString(),
                    ArticleName = r["ArticleName"].ToString(),
                    Summarize = r["Summarize"].ToString(),
                    ArticleContent = r["ArticleContent"].ToString(),
                    CategoryKey = r["CategoryKey"].ToInt(),
                    CategoryName = r["CategoryName"].ToString(),
                    ImageSmall = r["ImageSmall"].ToString(),
                    ImageLarge = r["ImageLarge"].ToString(),
                    FileAttack = r["FileAttack"].ToString(),
                    Publish = r["Publish"].ToBool(),
                    Hits = r["Hits"].ToInt(),
                    Rank = r["Rank"].ToInt(),
                    Activate = r["Activate"].ToBool(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }
        public static List<Article_Model> List(string PartnerNumber, int CategoryKey, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT ArticleKey, ArticleStatus, ArticleID, ArticleName, Summarize, CategoryKey, CategoryName, 
Publish, Hits, Rank, Activate, PartnerNumber, RecordStatus, CreatedOn, CreatedBy, CreatedName, ModifiedOn, ModifiedBy, ModifiedName
FROM PDT_Article 
WHERE RecordStatus != 99 
AND PartnerNumber = @PartnerNumber 
AND Slug = 0";
            if (CategoryKey != 0)
            {
                zSQL += " AND CategoryKey = @CategoryKey";
            }

            zSQL += " ORDER By CreatedOn DESC";
            string zConnectionString = TN_Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = CategoryKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
                Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Article_Model> zList = new List<Article_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Article_Model()
                {
                    ArticleKey = r["ArticleKey"].ToString(),
                    ArticleStatus = r["ArticleStatus"].ToInt(),
                    ArticleID = r["ArticleID"].ToString(),
                    ArticleName = r["ArticleName"].ToString(),
                    Summarize = r["Summarize"].ToString(),
                    CategoryKey = r["CategoryKey"].ToInt(),
                    CategoryName = r["CategoryName"].ToString(),                  
                    Publish = r["Publish"].ToBool(),
                    Hits = r["Hits"].ToInt(),
                    Rank = r["Rank"].ToInt(),
                    Activate = r["Activate"].ToBool(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }
        /// <summary>
        /// Show ra ngoài website
        /// </summary>
        /// <param name="PartnerNumber"></param>
        /// <param name="CategoryKey"></param>
        /// <param name="Message"></param>
        /// <returns></returns>
        public static List<Article_Model> ListShow(string PartnerNumber, int CategoryKey, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT ArticleKey, ArticleStatus, ArticleID, ArticleName, Summarize, CategoryKey, CategoryName, 
Publish, Hits, Rank, Activate, PartnerNumber, RecordStatus, ImageSmall, ImageLarge,
CreatedOn, CreatedBy, CreatedName, ModifiedOn, ModifiedBy, ModifiedName 
FROM PDT_Article 
WHERE RecordStatus != 99 
AND PartnerNumber = @PartnerNumber 
AND Publish = 'TRUE'";

            if (CategoryKey != 0)
            {
                zSQL += " AND CategoryKey = @CategoryKey";
            }

            zSQL += " ORDER By CreatedOn DESC";
            string zConnectionString = TN_Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = CategoryKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
                Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Article_Model> zList = new List<Article_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Article_Model()
                {
                    ArticleKey = r["ArticleKey"].ToString(),
                    ArticleStatus = r["ArticleStatus"].ToInt(),
                    ArticleID = r["ArticleID"].ToString(),
                    ArticleName = r["ArticleName"].ToString(),
                    Summarize = r["Summarize"].ToString(),                   
                    CategoryKey = r["CategoryKey"].ToInt(),
                    CategoryName = r["CategoryName"].ToString(),
                    ImageSmall = r["ImageSmall"].ToString(),
                    ImageLarge = r["ImageLarge"].ToString(),                  
                    Publish = r["Publish"].ToBool(),                  
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }
    }
}
