﻿using System;
using System.Collections.Generic;

namespace WebSite
{
    public class User_Model
    {
        #region [ Field Name ]
        private string _UserKey = "";
        private string _UserAPI = "";
        private string _UserName = "";
        private string _Password = "";
        private string _PIN = "";
        private string _Description = "";
        private string _GroupName = "";
        private string _PartnerNumber = "";
        private int? _BusinessKey = 0;
        private bool? _Activate;
        private DateTime? _ExpireDate = null;
        private DateTime? _LastLoginDate = null;
        private int? _FailedPasswordCount = 0;
        private int? _Slug = 0;
        private int? _RecordStatus = 0;
        private DateTime? _CreatedOn = null;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime? _ModifiedOn = null;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        #endregion

        #region [ Properties ]
        public string UserKey
        {
            get { return _UserKey; }
            set { _UserKey = value; }
        }
        public string UserAPI
        {
            get { return _UserAPI; }
            set { _UserAPI = value; }
        }
        public string UserName
        {
            get { return _UserName; }
            set { _UserName = value; }
        }
        public string Password
        {
            get { return _Password; }
            set { _Password = value; }
        }
        public string PIN
        {
            get { return _PIN; }
            set { _PIN = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public string GroupName
        {
            get { return _GroupName; }
            set { _GroupName = value; }
        }
        public string PartnerNumber
        {
            get { return _PartnerNumber; }
            set { _PartnerNumber = value; }
        }
        public int? BusinessKey
        {
            get { return _BusinessKey; }
            set { _BusinessKey = value; }
        }
        public bool? Activate
        {
            get { return _Activate; }
            set { _Activate = value; }
        }
        public DateTime? ExpireDate
        {
            get { return _ExpireDate; }
            set { _ExpireDate = value; }
        }
        public DateTime? LastLoginDate
        {
            get { return _LastLoginDate; }
            set { _LastLoginDate = value; }
        }
        public int? FailedPasswordCount
        {
            get { return _FailedPasswordCount; }
            set { _FailedPasswordCount = value; }
        }

        public int? Slug
        {
            get { return _Slug; }
            set { _Slug = value; }
        }
        public int? RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public DateTime? CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime? ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        #endregion
    }
}
