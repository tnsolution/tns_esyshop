﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
namespace WebSite
{
    public class Partner_Data
    {
        public static List<Partner_Model> List()
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM SYS_Partner WHERE RecordStatus != 99 ORDER BY PartnerID";
            string zConnectionString = TN_Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);                
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            List<Partner_Model> zList = new List<Partner_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Partner_Model()
                {
                    ModuleRole = r["ModuleRole"].ToString(),
                    ModuleRoleMobile = r["ModuleRoleMobile"].ToString(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    PartnerName = r["PartnerName"].ToString(),
                    PartnerAddress = r["PartnerAddress"].ToString(),
                    PartnerPhone = r["PartnerPhone"].ToString(),
                    PartnerID = r["PartnerID"].ToString(),
                    StoreID = r["StoreID"].ToString(),
                    StoreName = r["StoreName"].ToString(),
                    Parent = r["Parent"].ToString(),
                    Activated = r["Activated"].ToBool(),
                    ActivationDate = (r["ActivationDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ActivationDate"]),
                    Description = r["Description"].ToString(),
                    BusinessKey = r["BusinessKey"].ToInt(),
                    BusinessName = r["BusinessName"].ToString(),
                    Logo_Small = r["Logo_Small"].ToString(),
                    Logo_Large = r["Logo_Large"].ToString(),
                    Logo_Mobile = r["Logo_Mobile"].ToString(),
                    AccountAmount = r["AccountAmount"].ToInt(),
                    HomepageDesktop = r["HomepageDesktop"].ToString(),
                    HomepageMobile = r["HomepageMobile"].ToString(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }
        public static List<Partner_Model> List(string Name)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM SYS_Partner WHERE RecordStatus != 99";
            if (Name != string.Empty)
            {
                zSQL += " AND PartnerNumber = @Name";
            }
            zSQL += " ORDER BY PartnerName";
            string zConnectionString = TN_Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = "%" + Name + "%";
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            List<Partner_Model> zList = new List<Partner_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Partner_Model()
                {
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    PartnerName = r["PartnerName"].ToString(),
                    PartnerAddress = r["PartnerAddress"].ToString(),
                    PartnerPhone = r["PartnerPhone"].ToString(),
                    PartnerID = r["PartnerID"].ToString(),
                    StoreID = r["StoreID"].ToString(),
                    StoreName = r["StoreName"].ToString(),
                    Parent = r["Parent"].ToString(),
                    Activated = r["Activated"].ToBool(),
                    ActivationDate = (r["ActivationDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ActivationDate"]),
                    Description = r["Description"].ToString(),
                    BusinessKey = r["BusinessKey"].ToInt(),
                    BusinessName = r["BusinessName"].ToString(),
                    Logo_Small = r["Logo_Small"].ToString(),
                    Logo_Large = r["Logo_Large"].ToString(),
                    Logo_Mobile = r["Logo_Mobile"].ToString(),
                    AccountAmount = r["AccountAmount"].ToInt(),
                    HomepageDesktop = r["HomepageDesktop"].ToString(),
                    HomepageMobile = r["HomepageMobile"].ToString(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }
    }
}
