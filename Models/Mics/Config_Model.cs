﻿namespace WebSite
{
    public class Config_Model
    {
        #region [ Field Name ]
        private int _ConfigKey = 0;
        private string _ConfigID = "";
        private string _Name = "";
        private string _Value = "";
        private int _Parent = 0;
        private int _Rank = 0;
        private int _Slug = 0;
        private string _PartnerNumber = "";
        #endregion

        #region [ Properties ]
        public int ConfigKey
        {
            get { return _ConfigKey; }
            set { _ConfigKey = value; }
        }
        public string ConfigID
        {
            get { return _ConfigID; }
            set { _ConfigID = value; }
        }
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }
        public string Value
        {
            get { return _Value; }
            set { _Value = value; }
        }
        public int Parent
        {
            get { return _Parent; }
            set { _Parent = value; }
        }
        public int Rank
        {
            get { return _Rank; }
            set { _Rank = value; }
        }
        public string PartnerNumber
        {
            get { return _PartnerNumber; }
            set { _PartnerNumber = value; }
        }

        public int Slug { get => _Slug; set => _Slug = value; }
        #endregion
    }
}
