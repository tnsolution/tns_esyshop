﻿using System;
using System.Data;
using System.Data.SqlClient;
namespace WebSite
{
    public class Menu_WebSite_Info
    {

        public Menu_WebSite_Model Menu_WebSite = new Menu_WebSite_Model();
        private string _Message = "";
        public string Code
        {
            get
            {
                if (_Message.Length >= 3)
                    return _Message.Substring(0, 3);
                else return "";
            }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        #region [ Constructor Get Information ]
        public Menu_WebSite_Info()
        {
        }
        public Menu_WebSite_Info(int MenuKey)
        {
            string zSQL = "SELECT * FROM PDT_Menu_WebSite WHERE MenuKey = @MenuKey AND RecordStatus != 99 ";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@MenuKey", SqlDbType.Int).Value = MenuKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["MenuKey"] != DBNull.Value)
                        Menu_WebSite.MenuKey = int.Parse(zReader["MenuKey"].ToString());
                    Menu_WebSite.MenuID = zReader["MenuID"].ToString();
                    Menu_WebSite.MenuName = zReader["MenuName"].ToString();
                    if (zReader["Publish"] != DBNull.Value)
                        Menu_WebSite.Publish = (bool)zReader["Publish"];
                    if (zReader["Parent"] != DBNull.Value)
                        Menu_WebSite.Parent = int.Parse(zReader["Parent"].ToString());
                    if (zReader["Rank"] != DBNull.Value)
                        Menu_WebSite.Rank = int.Parse(zReader["Rank"].ToString());
                    Menu_WebSite.Action = zReader["Action"].ToString();
                    Menu_WebSite.Paramater = zReader["Paramater"].ToString();
                    if (zReader["TypeKey"] != DBNull.Value)
                        Menu_WebSite.TypeKey = int.Parse(zReader["TypeKey"].ToString());
                    Menu_WebSite.TypeName = zReader["TypeName"].ToString();
                    Menu_WebSite.PartnerNumber = zReader["PartnerNumber"].ToString();
                    if (zReader["RecordStatus"] != DBNull.Value)
                        Menu_WebSite.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    Menu_WebSite.ContentTitle = zReader["ContentTitle"].ToString();
                    _Message = "200 OK";
                }
                else
                {
                    _Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }

        #endregion

        #region [ Constructor Update Information ]

        public string Create_ServerKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO PDT_Menu_WebSite ("
         + " MenuID , MenuName , Publish , Parent , Rank , Action , Paramater , TypeKey , TypeName , PartnerNumber , RecordStatus, ContentTitle ) "
         + " VALUES ( "
         + " @MenuID , @MenuName , @Publish , @Parent , @Rank , @Action , @Paramater , @TypeKey , @TypeName , @PartnerNumber , @RecordStatus, @ContentTitle ) ";
            string zResult = "";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@MenuID", SqlDbType.NVarChar).Value = Menu_WebSite.MenuID;
                zCommand.Parameters.Add("@MenuName", SqlDbType.NVarChar).Value = Menu_WebSite.MenuName;
                zCommand.Parameters.Add("@ContentTitle", SqlDbType.NVarChar).Value = Menu_WebSite.ContentTitle;
                zCommand.Parameters.Add("@Publish", SqlDbType.Bit).Value = Menu_WebSite.Publish;
                zCommand.Parameters.Add("@Parent", SqlDbType.Int).Value = Menu_WebSite.Parent;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = Menu_WebSite.Rank;
                zCommand.Parameters.Add("@Action", SqlDbType.NVarChar).Value = Menu_WebSite.Action;
                zCommand.Parameters.Add("@Paramater", SqlDbType.NVarChar).Value = Menu_WebSite.Paramater;
                zCommand.Parameters.Add("@TypeKey", SqlDbType.Int).Value = Menu_WebSite.TypeKey;
                zCommand.Parameters.Add("@TypeName", SqlDbType.NVarChar).Value = Menu_WebSite.TypeName;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = Menu_WebSite.PartnerNumber;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Menu_WebSite.RecordStatus;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        public string Update()
        {
            string zSQL = "UPDATE PDT_Menu_WebSite SET"
                        + " MenuID = @MenuID,"
                        + " MenuName = @MenuName,"
                        + " Publish = @Publish,"
                        + " Parent = @Parent,"
                        + " Rank = @Rank,"
                        + " Action = @Action,"
                        + " Paramater = @Paramater,"
                        + " TypeKey = @TypeKey,"
                        + " TypeName = @TypeName,"
                        + " PartnerNumber = @PartnerNumber,"
                        + " RecordStatus = @RecordStatus,"
                        + " ContentTitle = ContentTitle"
                        + " WHERE MenuKey = @MenuKey";
            string zResult = "";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@MenuKey", SqlDbType.Int).Value = Menu_WebSite.MenuKey;
                zCommand.Parameters.Add("@ContentTitle", SqlDbType.NVarChar).Value = Menu_WebSite.ContentTitle;

                zCommand.Parameters.Add("@MenuID", SqlDbType.NVarChar).Value = Menu_WebSite.MenuID;
                zCommand.Parameters.Add("@MenuName", SqlDbType.NVarChar).Value = Menu_WebSite.MenuName;
                zCommand.Parameters.Add("@Publish", SqlDbType.Bit).Value = Menu_WebSite.Publish;
                zCommand.Parameters.Add("@Parent", SqlDbType.Int).Value = Menu_WebSite.Parent;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = Menu_WebSite.Rank;
                zCommand.Parameters.Add("@Action", SqlDbType.NVarChar).Value = Menu_WebSite.Action;
                zCommand.Parameters.Add("@Paramater", SqlDbType.NVarChar).Value = Menu_WebSite.Paramater;
                zCommand.Parameters.Add("@TypeKey", SqlDbType.Int).Value = Menu_WebSite.TypeKey;
                zCommand.Parameters.Add("@TypeName", SqlDbType.NVarChar).Value = Menu_WebSite.TypeName;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = Menu_WebSite.PartnerNumber;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Menu_WebSite.RecordStatus;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE PDT_Menu_WebSite SET RecordStatus = 99 WHERE MenuKey = @MenuKey";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@MenuKey", SqlDbType.Int).Value = Menu_WebSite.MenuKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string SetActivate(int MenuKey)
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE PDT_Menu_WebSite SET Publish = (CASE Publish WHEN 'true' THEN 'false' ELSE 'true' END) WHERE MenuKey = @MenuKey";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@MenuKey", SqlDbType.Int).Value = MenuKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
