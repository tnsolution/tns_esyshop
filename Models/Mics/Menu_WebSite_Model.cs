﻿namespace WebSite
{
    public class Menu_WebSite_Model
    {
        #region [ Field Name ]
        private int _MenuKey = 0;
        private string _MenuID = "";
        private string _MenuName = "";
        private bool _Publish;
        private int _Parent = 0;
        private int _Rank = 0;
        private string _Action = "";
        private string _Paramater = "";
        private int _TypeKey = 0;
        private string _TypeName = "";
        private string _PartnerNumber = "";
        private int _RecordStatus = 0;
        private string _ContentTitle = "";
        #endregion

        #region [ Properties ]
        public int MenuKey
        {
            get { return _MenuKey; }
            set { _MenuKey = value; }
        }
        public string MenuID
        {
            get { return _MenuID; }
            set { _MenuID = value; }
        }
        public string MenuName
        {
            get { return _MenuName; }
            set { _MenuName = value; }
        }
        public bool Publish
        {
            get { return _Publish; }
            set { _Publish = value; }
        }
        public int Parent
        {
            get { return _Parent; }
            set { _Parent = value; }
        }
        public int Rank
        {
            get { return _Rank; }
            set { _Rank = value; }
        }
        public string Action
        {
            get { return _Action; }
            set { _Action = value; }
        }
        public string Paramater
        {
            get { return _Paramater; }
            set { _Paramater = value; }
        }
        public int TypeKey
        {
            get { return _TypeKey; }
            set { _TypeKey = value; }
        }
        public string TypeName
        {
            get { return _TypeName; }
            set { _TypeName = value; }
        }
        public string PartnerNumber
        {
            get { return _PartnerNumber; }
            set { _PartnerNumber = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }

        public string ContentTitle { get => _ContentTitle; set => _ContentTitle = value; }
        #endregion
    }
}
