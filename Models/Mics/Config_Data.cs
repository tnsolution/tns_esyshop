﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
namespace WebSite
{
    public class Config_Data
    {
        public static List<Config_Model> List(string PartnerNumber, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = "WITH R AS ( SELECT ConfigKey, ConfigID, Name, Value, PARENT, Rank, DEPTH = 0, SORT = CAST(ConfigKey AS VARCHAR(MAX)) "
            + " FROM SYS_Config "
            + " WHERE RecordStatus <> 99 "
            + " AND Parent = 0 "
            + " AND PartnerNumber = @PartnerNumber "
            + " UNION ALL "
            + " SELECT Sub.ConfigKey, Sub.ConfigID, Sub.Name, Sub.Value, Sub.PARENT, Sub.Rank, DEPTH = R.DEPTH + 1, SORT = R.SORT + '-' + CAST(Sub.ConfigKey AS VARCHAR(MAX)) "
            + " FROM R INNER JOIN SYS_Config Sub ON R.ConfigKey = Sub.Parent "
            + " WHERE Sub.RecordStatus <> 99 "
            + " AND Sub.PartnerNumber = @PartnerNumber) "
            + " SELECT Name = REPLICATE('---', R.DEPTH * 1) + R.[Name], R.ConfigKey, R.ConfigID, dbo.WebSite_GetArticleName(R.Value) AS Value , R.SORT, R.DEPTH, R.Parent, R.Rank FROM R ORDER BY SORT";
            string zConnectionString = TN_Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Config_Model> zList = new List<Config_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Config_Model()
                {
                    ConfigKey = r["ConfigKey"].ToInt(),
                    ConfigID = r["ConfigID"].ToString(),
                    Name = r["Name"].ToString(),
                    Value = r["Value"].ToString(),
                    Parent = r["Parent"].ToInt(),
                    Rank = r["Rank"].ToInt(),
                });
            }
            return zList;
        }

        public static List<Config_Model> List(string PartnerNumber, int Parent, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM SYS_Config WHERE PartnerNumber = @PartnerNumber AND RecordStatus <> 99 AND Parent = @Parent";
            string zConnectionString = TN_Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@Parent", SqlDbType.Int).Value = Parent;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
                Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Config_Model> zList = new List<Config_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Config_Model()
                {
                    ConfigKey = r["ConfigKey"].ToInt(),
                    ConfigID = r["ConfigID"].ToString(),
                    Name = r["Name"].ToString(),
                    Value = r["Value"].ToString(),
                    Parent = r["Parent"].ToInt(),
                    Rank = r["Rank"].ToInt(),
                });
            }
            return zList;
        }
    }
}