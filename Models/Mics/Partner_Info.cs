﻿using System;
using System.Data;
using System.Data.SqlClient;
namespace WebSite
{
    public class Partner_Info
    {
        public Partner_Model Partner = new Partner_Model();
        private string _Message = "";
        public string Code
        {
            get
            {
                if (_Message.Length >= 3)
                {
                    return _Message.Substring(0, 3);
                }
                else
                {
                    return "";
                }
            }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        #region [ Constructor Get Information ]
        public Partner_Info()
        {
            Partner.PartnerNumber = Guid.NewGuid().ToString();
        }
        public Partner_Info(string PartnerNumber)
        {
            string zSQL = "SELECT * FROM SYS_Partner WHERE PartnerNumber = @PartnerNumber AND RecordStatus != 99 ";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(PartnerNumber);
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    Partner.BillConfig = zReader["BillConfig"].ToString();
                    Partner.ModuleRole = zReader["ModuleRole"].ToString();
                    Partner.ModuleRoleMobile = zReader["ModuleRoleMobile"].ToString();

                    Partner.PartnerNumber = zReader["PartnerNumber"].ToString();
                    Partner.PartnerName = zReader["PartnerName"].ToString();
                    Partner.PartnerAddress = zReader["PartnerAddress"].ToString();
                    Partner.PartnerPhone = zReader["PartnerPhone"].ToString();
                    Partner.PartnerID = zReader["PartnerID"].ToString();
                    Partner.StoreID = zReader["StoreID"].ToString();
                    Partner.StoreName = zReader["StoreName"].ToString();
                    Partner.Parent = zReader["Parent"].ToString();
                    if (zReader["Activated"] != DBNull.Value)
                    {
                        Partner.Activated = (bool)zReader["Activated"];
                    }

                    if (zReader["ActivationDate"] != DBNull.Value)
                    {
                        Partner.ActivationDate = (DateTime)zReader["ActivationDate"];
                    }

                    Partner.Description = zReader["Description"].ToString();
                    if (zReader["BusinessKey"] != DBNull.Value)
                    {
                        Partner.BusinessKey = int.Parse(zReader["BusinessKey"].ToString());
                    }

                    Partner.BusinessName = zReader["BusinessName"].ToString();
                    Partner.Logo_Small = zReader["Logo_Small"].ToString();
                    Partner.Logo_Large = zReader["Logo_Large"].ToString();
                    Partner.Logo_Mobile = zReader["Logo_Mobile"].ToString();
                    if (zReader["AccountAmount"] != DBNull.Value)
                    {
                        Partner.AccountAmount = int.Parse(zReader["AccountAmount"].ToString());
                    }

                    Partner.HomepageDesktop = zReader["HomepageDesktop"].ToString();
                    Partner.HomepageMobile = zReader["HomepageMobile"].ToString();
                    if (zReader["RecordStatus"] != DBNull.Value)
                    {
                        Partner.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    }

                    if (zReader["CreatedOn"] != DBNull.Value)
                    {
                        Partner.CreatedOn = (DateTime)zReader["CreatedOn"];
                    }

                    Partner.CreatedBy = zReader["CreatedBy"].ToString();
                    Partner.CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                    {
                        Partner.ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    }

                    Partner.ModifiedBy = zReader["ModifiedBy"].ToString();
                    Partner.ModifiedName = zReader["ModifiedName"].ToString();
                    _Message = "200 OK";
                }
                else
                {
                    _Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }
        #endregion

        #region [ Constructor Update Information ]            
        public string ChangeLogo()
        {
            string zSQL = "UPDATE SYS_Partner SET "
                        + " Logo_Large = @Logo_Large,"
                        + " ModifiedOn = GetDate(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                        + " WHERE PartnerNumber = @PartnerNumber";
            string zResult = "";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Partner.PartnerNumber);
                zCommand.Parameters.Add("@Logo_Large", SqlDbType.NVarChar).Value = Partner.Logo_Large;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Partner.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Partner.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}