﻿using System;
namespace WebSite
{
    public class Partner_Model
    {
        #region [ Field Name ]
        private string _BillConfig = "";
        private string _UserName = "";
        private string _Password = "";
        private string _PartnerNumber = "";
        private string _PartnerName = "";
        private string _PartnerAddress = "";
        private string _PartnerPhone = "";
        private string _PartnerID = "";
        private string _StoreID = "";
        private string _StoreName = "";
        private string _ModuleRole = "";
        private string _ModuleRoleMobile = "";
        private string _Parent = "";
        private bool _Activated = true;
        private DateTime _ActivationDate = DateTime.MinValue;
        private string _Description = "";
        private int _BusinessKey = 0;
        private string _BusinessName = "";
        private string _Logo_Small = "";
        private string _Logo_Large = "";
        private string _Logo_Mobile = "";
        private int _AccountAmount = 0;
        private string _HomepageDesktop = "";
        private string _HomepageMobile = "";
        private int _RecordStatus = 0;
        private DateTime _CreatedOn = DateTime.MinValue;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedOn = DateTime.MinValue;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        #endregion

        #region [ Properties ]
        public string PartnerNumber
        {
            get { return _PartnerNumber; }
            set { _PartnerNumber = value; }
        }
        public string PartnerName
        {
            get { return _PartnerName; }
            set { _PartnerName = value; }
        }
        public string PartnerAddress
        {
            get { return _PartnerAddress; }
            set { _PartnerAddress = value; }
        }
        public string PartnerPhone
        {
            get { return _PartnerPhone; }
            set { _PartnerPhone = value; }
        }
        public string PartnerID
        {
            get { return _PartnerID; }
            set { _PartnerID = value; }
        }
        public string StoreID
        {
            get { return _StoreID; }
            set { _StoreID = value; }
        }
        public string StoreName
        {
            get { return _StoreName; }
            set { _StoreName = value; }
        }
        public string Parent
        {
            get { return _Parent; }
            set { _Parent = value; }
        }
        public bool Activated
        {
            get { return _Activated; }
            set { _Activated = value; }
        }
        public DateTime ActivationDate
        {
            get { return _ActivationDate; }
            set { _ActivationDate = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public int BusinessKey
        {
            get { return _BusinessKey; }
            set { _BusinessKey = value; }
        }
        public string BusinessName
        {
            get { return _BusinessName; }
            set { _BusinessName = value; }
        }
        public string Logo_Small
        {
            get { return _Logo_Small; }
            set { _Logo_Small = value; }
        }
        public string Logo_Large
        {
            get { return _Logo_Large; }
            set { _Logo_Large = value; }
        }
        public string Logo_Mobile
        {
            get { return _Logo_Mobile; }
            set { _Logo_Mobile = value; }
        }
        public int AccountAmount
        {
            get { return _AccountAmount; }
            set { _AccountAmount = value; }
        }
        public string HomepageDesktop
        {
            get { return _HomepageDesktop; }
            set { _HomepageDesktop = value; }
        }
        public string HomepageMobile
        {
            get { return _HomepageMobile; }
            set { _HomepageMobile = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }

        public string ModuleRole
        {
            get
            {
                return _ModuleRole;
            }

            set
            {
                _ModuleRole = value;
            }
        }

        public string ModuleRoleMobile
        {
            get
            {
                return _ModuleRoleMobile;
            }

            set
            {
                _ModuleRoleMobile = value;
            }
        }

        public string UserName
        {
            get
            {
                return _UserName;
            }

            set
            {
                _UserName = value;
            }
        }

        public string Password
        {
            get
            {
                return _Password;
            }

            set
            {
                _Password = value;
            }
        }

        public string BillConfig
        {
            get
            {
                return _BillConfig;
            }

            set
            {
                _BillConfig = value;
            }
        }
        #endregion
    }
}
