﻿using System;
namespace WebSite
{
    public class Order_Item_Model
    {
        #region [ Field Name ]
        private string _PhotoPath = "";
        private int _AutoKey = 0;
        private string _OrderKey = "";
        private string _ObjectTable = "";
        private string _ObjectKey = "";
        private string _ObjectName = "";
        private double _ObjectPrice = 0;
        private float _Quantity = 0;
        private int _UnitKey = 0;
        private string _UnitName = "";
        private double _Amount = 0;
        private double _AmountDiscount = 0;
        private float _PercentDiscount = 0;
        private double _AmountNoVAT = 0;
        private float _VAT = 0;
        private double _AmountVAT = 0;
        private double _AmountIncVAT = 0;
        private string _Description = "";
        private string _PartnerNumber = "";
        private int _RecordStatus = 0;
        private DateTime _CreatedOn = DateTime.MinValue;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedOn = DateTime.MinValue;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        #endregion

        #region [ Properties ]
        public int AutoKey
        {
            get { return _AutoKey; }
            set { _AutoKey = value; }
        }
        public string OrderKey
        {
            get { return _OrderKey; }
            set { _OrderKey = value; }
        }
        public string ObjectTable
        {
            get { return _ObjectTable; }
            set { _ObjectTable = value; }
        }
        public string ObjectKey
        {
            get { return _ObjectKey; }
            set { _ObjectKey = value; }
        }
        public string ObjectName
        {
            get { return _ObjectName; }
            set { _ObjectName = value; }
        }
        public double ObjectPrice
        {
            get { return _ObjectPrice; }
            set { _ObjectPrice = value; }
        }
        public float Quantity
        {
            get { return _Quantity; }
            set { _Quantity = value; }
        }
        public int UnitKey
        {
            get { return _UnitKey; }
            set { _UnitKey = value; }
        }
        public string UnitName
        {
            get { return _UnitName; }
            set { _UnitName = value; }
        }
        public double Amount
        {
            get { return _Amount; }
            set { _Amount = value; }
        }
        public double AmountDiscount
        {
            get { return _AmountDiscount; }
            set { _AmountDiscount = value; }
        }
        public float PercentDiscount
        {
            get { return _PercentDiscount; }
            set { _PercentDiscount = value; }
        }
        public double AmountNoVAT
        {
            get { return _AmountNoVAT; }
            set { _AmountNoVAT = value; }
        }
        public float VAT
        {
            get { return _VAT; }
            set { _VAT = value; }
        }
        public double AmountVAT
        {
            get { return _AmountVAT; }
            set { _AmountVAT = value; }
        }
        public double AmountIncVAT
        {
            get { return _AmountIncVAT; }
            set { _AmountIncVAT = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public string PartnerNumber
        {
            get { return _PartnerNumber; }
            set { _PartnerNumber = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }

        public string PhotoPath { get => _PhotoPath; set => _PhotoPath = value; }
        #endregion
    }
}
