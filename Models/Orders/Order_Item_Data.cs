﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
namespace WebSite
{
    public class Order_Item_Data
    {
        public static List<Order_Item_Model> List(string PartnerNumber, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM FNC_Order_Item WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ";
            string zConnectionString = TN_Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Order_Item_Model> zList = new List<Order_Item_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Order_Item_Model()
                {
                    AutoKey = r["AutoKey"].ToInt(),
                    OrderKey = r["OrderKey"].ToString(),
                    ObjectTable = r["ObjectTable"].ToString(),
                    ObjectKey = r["ObjectKey"].ToString(),
                    ObjectName = r["ObjectName"].ToString(),
                    ObjectPrice = r["ObjectPrice"].ToDouble(),
                    Quantity = r["Quantity"].ToFloat(),
                    UnitKey = r["UnitKey"].ToInt(),
                    UnitName = r["UnitName"].ToString(),
                    Amount = r["Amount"].ToDouble(),
                    AmountDiscount = r["AmountDiscount"].ToDouble(),
                    PercentDiscount = r["PercentDiscount"].ToFloat(),
                    AmountNoVAT = r["AmountNoVAT"].ToDouble(),
                    VAT = r["VAT"].ToFloat(),
                    AmountVAT = r["AmountVAT"].ToDouble(),
                    AmountIncVAT = r["AmountIncVAT"].ToDouble(),
                    Description = r["Description"].ToString(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }
    }
}
