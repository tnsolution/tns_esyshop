﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
namespace WebSite
{
    public class Order_Data
    {
        public static List<Order_Model> Search(string PartnerNumber, string Name, string Phone, string ID, int Status, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM FNC_Order WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ";
            if (!string.IsNullOrEmpty(Name))
            {
                zSQL += " AND BuyerName LIKE @Name";
            }
            if (!string.IsNullOrEmpty(Phone))
            {
                zSQL += " AND BuyerPhone LIKE @Phone";
            }
            if (!string.IsNullOrEmpty(ID))
            {
                zSQL += " AND OrderID = @ID";
            }
            if (Status != 0)
            {
                zSQL += " AND StatusOrder = @Status";
            }
            zSQL += " ORDER BY CreatedOn DESC";
            string zConnectionString = TN_Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zCommand.Parameters.Add("@Status", SqlDbType.Int).Value = Status;
                if (!string.IsNullOrEmpty(Name))
                    zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = "%" + Name + "%";
                if (!string.IsNullOrEmpty(Phone))
                    zCommand.Parameters.Add("@Phone", SqlDbType.NVarChar).Value = "%" + Phone + "%"; ;
                if (!string.IsNullOrEmpty(ID))
                    zCommand.Parameters.Add("@ID", SqlDbType.NVarChar).Value = ID;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Order_Model> zList = new List<Order_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Order_Model()
                {
                    OrderKey = r["OrderKey"].ToString(),
                    OrderID = r["OrderID"].ToString(),
                    OrderDate = (r["OrderDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["OrderDate"]),
                    StatusOrder = r["StatusOrder"].ToInt(),
                    StatusName = r["StatusName"].ToString(),
                    Description = r["Description"].ToString(),
                    ObjectKey = r["ObjectKey"].ToString(),
                    ObjectName = r["ObjectName"].ToString(),
                    SellerKey = r["SellerKey"].ToString(),
                    SellerName = r["SellerName"].ToString(),
                    BuyerKey = r["BuyerKey"].ToString(),
                    BuyerName = r["BuyerName"].ToString(),
                    BuyerPhone = r["BuyerPhone"].ToString(),
                    BuyerAddress = r["BuyerAddress"].ToString(),
                    BuyerEmail = r["BuyerEmail"].ToString(),
                    ShippingAddress = r["ShippingAddress"].ToString(),
                    ShippingCity = r["ShippingCity"].ToString(),
                    WarehouseKey = r["WarehouseKey"].ToString(),
                    WarehouseName = r["WarehouseName"].ToString(),
                    TotalQuantity = r["TotalQuantity"].ToFloat(),
                    AmountOrder = r["AmountOrder"].ToDouble(),
                    AmountDiscount = r["AmountDiscount"].ToDouble(),
                    PercentDiscount = r["PercentDiscount"].ToFloat(),
                    AmountOrderNoVAT = r["AmountOrderNoVAT"].ToDouble(),
                    VAT = r["VAT"].ToFloat(),
                    AmountVAT = r["AmountVAT"].ToDouble(),
                    AmountOrderIncVAT = r["AmountOrderIncVAT"].ToDouble(),
                    AmountReceived = r["AmountReceived"].ToDouble(),
                    AmountReturned = r["AmountReturned"].ToDouble(),
                    CategoryKey = r["CategoryKey"].ToInt(),
                    CategoryName = r["CategoryName"].ToString(),
                    Slug = r["Slug"].ToInt(),
                    CreditAccount = r["CreditAccount"].ToString(),
                    DebitAccount = r["DebitAccount"].ToString(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    Organization = r["Organization"].ToString(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }
        public static string AutoID(string Prefix, string PartnerNumber)
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.Auto_OrderID(@Prefix, @PartnerNumber)";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Prefix", SqlDbType.NVarChar).Value = Prefix;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                zResult = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public static List<Order_Model> List(string PartnerNumber, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM FNC_Order WHERE RecordStatus != 99 AND PartnerNumber = @PartnerNumber ";
            zSQL += " ORDER BY CreatedOn DESC";
            string zConnectionString = TN_Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Order_Model> zList = new List<Order_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Order_Model()
                {
                    OrderKey = r["OrderKey"].ToString(),
                    OrderID = r["OrderID"].ToString(),
                    OrderDate = (r["OrderDate"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["OrderDate"]),
                    StatusOrder = r["StatusOrder"].ToInt(),
                    StatusName = r["StatusName"].ToString(),
                    Description = r["Description"].ToString(),
                    ObjectKey = r["ObjectKey"].ToString(),
                    ObjectName = r["ObjectName"].ToString(),
                    SellerKey = r["SellerKey"].ToString(),
                    SellerName = r["SellerName"].ToString(),
                    BuyerKey = r["BuyerKey"].ToString(),
                    BuyerName = r["BuyerName"].ToString(),
                    BuyerPhone = r["BuyerPhone"].ToString(),
                    BuyerAddress = r["BuyerAddress"].ToString(),
                    BuyerEmail = r["BuyerEmail"].ToString(),
                    ShippingAddress = r["ShippingAddress"].ToString(),
                    ShippingCity = r["ShippingCity"].ToString(),
                    WarehouseKey = r["WarehouseKey"].ToString(),
                    WarehouseName = r["WarehouseName"].ToString(),
                    TotalQuantity = r["TotalQuantity"].ToFloat(),
                    AmountOrder = r["AmountOrder"].ToDouble(),
                    AmountDiscount = r["AmountDiscount"].ToDouble(),
                    PercentDiscount = r["PercentDiscount"].ToFloat(),
                    AmountOrderNoVAT = r["AmountOrderNoVAT"].ToDouble(),
                    VAT = r["VAT"].ToFloat(),
                    AmountVAT = r["AmountVAT"].ToDouble(),
                    AmountOrderIncVAT = r["AmountOrderIncVAT"].ToDouble(),
                    AmountReceived = r["AmountReceived"].ToDouble(),
                    AmountReturned = r["AmountReturned"].ToDouble(),
                    CategoryKey = r["CategoryKey"].ToInt(),
                    CategoryName = r["CategoryName"].ToString(),
                    Slug = r["Slug"].ToInt(),
                    CreditAccount = r["CreditAccount"].ToString(),
                    DebitAccount = r["DebitAccount"].ToString(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    Organization = r["Organization"].ToString(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }
        public static List<Order_Item_Model> ListDetail(string OrderKey, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM FNC_Order_Item WHERE RecordStatus != 99 AND OrderKey = @OrderKey";
            string zConnectionString = TN_Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@OrderKey", SqlDbType.NVarChar).Value = OrderKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Order_Item_Model> zList = new List<Order_Item_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Order_Item_Model()
                {
                    AutoKey = r["AutoKey"].ToInt(),
                    OrderKey = r["OrderKey"].ToString(),
                    ObjectTable = r["ObjectTable"].ToString(),
                    ObjectKey = r["ObjectKey"].ToString(),
                    ObjectName = r["ObjectName"].ToString(),
                    ObjectPrice = r["ObjectPrice"].ToDouble(),
                    Quantity = r["Quantity"].ToFloat(),
                    UnitKey = r["UnitKey"].ToInt(),
                    UnitName = r["UnitName"].ToString(),
                    Amount = r["Amount"].ToDouble(),
                    AmountDiscount = r["AmountDiscount"].ToDouble(),
                    PercentDiscount = r["PercentDiscount"].ToFloat(),
                    AmountNoVAT = r["AmountNoVAT"].ToDouble(),
                    VAT = r["VAT"].ToFloat(),
                    AmountVAT = r["AmountVAT"].ToDouble(),
                    AmountIncVAT = r["AmountIncVAT"].ToDouble(),
                    Description = r["Description"].ToString(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    CreatedOn = (r["CreatedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["CreatedOn"]),
                    CreatedBy = r["CreatedBy"].ToString(),
                    CreatedName = r["CreatedName"].ToString(),
                    ModifiedOn = (r["ModifiedOn"] == DBNull.Value ? DateTime.MinValue : (DateTime)r["ModifiedOn"]),
                    ModifiedBy = r["ModifiedBy"].ToString(),
                    ModifiedName = r["ModifiedName"].ToString(),
                });
            }
            return zList;
        }
    }
}