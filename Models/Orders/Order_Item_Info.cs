﻿using System;
using System.Data;
using System.Data.SqlClient;
namespace WebSite
{
    public class Order_Item_Info
    {

        public Order_Item_Model Order_Item = new Order_Item_Model();
        private string _Message = "";
        public string Code
        {
            get
            {
                if (_Message.Length >= 3)
                    return _Message.Substring(0, 3);
                else return "";
            }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        #region [ Constructor Get Information ]
        public Order_Item_Info()
        {
        }
        public Order_Item_Info(int AutoKey)
        {
            string zSQL = "SELECT * FROM FNC_Order_Item WHERE AutoKey = @AutoKey AND RecordStatus != 99 ";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = AutoKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AutoKey"] != DBNull.Value)
                        Order_Item.AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    Order_Item.OrderKey = zReader["OrderKey"].ToString();
                    Order_Item.ObjectTable = zReader["ObjectTable"].ToString();
                    Order_Item.ObjectKey = zReader["ObjectKey"].ToString();
                    Order_Item.ObjectName = zReader["ObjectName"].ToString();
                    if (zReader["ObjectPrice"] != DBNull.Value)
                        Order_Item.ObjectPrice = double.Parse(zReader["ObjectPrice"].ToString());
                    if (zReader["Quantity"] != DBNull.Value)
                        Order_Item.Quantity = float.Parse(zReader["Quantity"].ToString());
                    if (zReader["UnitKey"] != DBNull.Value)
                        Order_Item.UnitKey = int.Parse(zReader["UnitKey"].ToString());
                    Order_Item.UnitName = zReader["UnitName"].ToString();
                    if (zReader["Amount"] != DBNull.Value)
                        Order_Item.Amount = double.Parse(zReader["Amount"].ToString());
                    if (zReader["AmountDiscount"] != DBNull.Value)
                        Order_Item.AmountDiscount = double.Parse(zReader["AmountDiscount"].ToString());
                    if (zReader["PercentDiscount"] != DBNull.Value)
                        Order_Item.PercentDiscount = float.Parse(zReader["PercentDiscount"].ToString());
                    if (zReader["AmountNoVAT"] != DBNull.Value)
                        Order_Item.AmountNoVAT = double.Parse(zReader["AmountNoVAT"].ToString());
                    if (zReader["VAT"] != DBNull.Value)
                        Order_Item.VAT = float.Parse(zReader["VAT"].ToString());
                    if (zReader["AmountVAT"] != DBNull.Value)
                        Order_Item.AmountVAT = double.Parse(zReader["AmountVAT"].ToString());
                    if (zReader["AmountIncVAT"] != DBNull.Value)
                        Order_Item.AmountIncVAT = double.Parse(zReader["AmountIncVAT"].ToString());
                    Order_Item.Description = zReader["Description"].ToString();
                    Order_Item.PartnerNumber = zReader["PartnerNumber"].ToString();
                    if (zReader["RecordStatus"] != DBNull.Value)
                        Order_Item.RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    if (zReader["CreatedOn"] != DBNull.Value)
                        Order_Item.CreatedOn = (DateTime)zReader["CreatedOn"];
                    Order_Item.CreatedBy = zReader["CreatedBy"].ToString();
                    Order_Item.CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        Order_Item.ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    Order_Item.ModifiedBy = zReader["ModifiedBy"].ToString();
                    Order_Item.ModifiedName = zReader["ModifiedName"].ToString();
                    _Message = "200 OK";
                }
                else
                {
                    _Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }

        #endregion

        #region [ Constructor Update Information ]

        public string Create_ServerKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO FNC_Order_Item ("
         + " OrderKey , ObjectTable , ObjectKey , ObjectName , ObjectPrice , Quantity , UnitKey , UnitName , Amount , AmountDiscount , PercentDiscount , AmountNoVAT , VAT , AmountVAT , AmountIncVAT , Description , PartnerNumber , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @OrderKey , @ObjectTable , @ObjectKey , @ObjectName , @ObjectPrice , @Quantity , @UnitKey , @UnitName , @Amount , @AmountDiscount , @PercentDiscount , @AmountNoVAT , @VAT , @AmountVAT , @AmountIncVAT , @Description , @PartnerNumber , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@OrderKey", SqlDbType.NVarChar).Value = Order_Item.OrderKey;
                zCommand.Parameters.Add("@ObjectTable", SqlDbType.NVarChar).Value = Order_Item.ObjectTable;
                zCommand.Parameters.Add("@ObjectKey", SqlDbType.NVarChar).Value = Order_Item.ObjectKey;
                zCommand.Parameters.Add("@ObjectName", SqlDbType.NVarChar).Value = Order_Item.ObjectName;
                zCommand.Parameters.Add("@ObjectPrice", SqlDbType.Money).Value = Order_Item.ObjectPrice;
                zCommand.Parameters.Add("@Quantity", SqlDbType.Float).Value = Order_Item.Quantity;
                zCommand.Parameters.Add("@UnitKey", SqlDbType.Int).Value = Order_Item.UnitKey;
                zCommand.Parameters.Add("@UnitName", SqlDbType.NVarChar).Value = Order_Item.UnitName;
                zCommand.Parameters.Add("@Amount", SqlDbType.Money).Value = Order_Item.Amount;
                zCommand.Parameters.Add("@AmountDiscount", SqlDbType.Money).Value = Order_Item.AmountDiscount;
                zCommand.Parameters.Add("@PercentDiscount", SqlDbType.Float).Value = Order_Item.PercentDiscount;
                zCommand.Parameters.Add("@AmountNoVAT", SqlDbType.Money).Value = Order_Item.AmountNoVAT;
                zCommand.Parameters.Add("@VAT", SqlDbType.Float).Value = Order_Item.VAT;
                zCommand.Parameters.Add("@AmountVAT", SqlDbType.Money).Value = Order_Item.AmountVAT;
                zCommand.Parameters.Add("@AmountIncVAT", SqlDbType.Money).Value = Order_Item.AmountIncVAT;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Order_Item.Description;
                if (Order_Item.PartnerNumber != "" && Order_Item.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Order_Item.PartnerNumber);
                }
                else
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Order_Item.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Order_Item.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Order_Item.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Order_Item.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Order_Item.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Create_ClientKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO FNC_Order_Item("
         + " AutoKey , OrderKey , ObjectTable , ObjectKey , ObjectName , ObjectPrice , Quantity , UnitKey , UnitName , Amount , AmountDiscount , PercentDiscount , AmountNoVAT , VAT , AmountVAT , AmountIncVAT , Description , PartnerNumber , RecordStatus , CreatedBy , CreatedName , ModifiedBy , ModifiedName ) "
         + " VALUES ( "
         + " @AutoKey , @OrderKey , @ObjectTable , @ObjectKey , @ObjectName , @ObjectPrice , @Quantity , @UnitKey , @UnitName , @Amount , @AmountDiscount , @PercentDiscount , @AmountNoVAT , @VAT , @AmountVAT , @AmountIncVAT , @Description , @PartnerNumber , @RecordStatus , @CreatedBy , @CreatedName , @ModifiedBy , @ModifiedName ) ";
            string zResult = "";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Order_Item.AutoKey;
                zCommand.Parameters.Add("@OrderKey", SqlDbType.NVarChar).Value = Order_Item.OrderKey;
                zCommand.Parameters.Add("@ObjectTable", SqlDbType.NVarChar).Value = Order_Item.ObjectTable;
                zCommand.Parameters.Add("@ObjectKey", SqlDbType.NVarChar).Value = Order_Item.ObjectKey;
                zCommand.Parameters.Add("@ObjectName", SqlDbType.NVarChar).Value = Order_Item.ObjectName;
                zCommand.Parameters.Add("@ObjectPrice", SqlDbType.Money).Value = Order_Item.ObjectPrice;
                zCommand.Parameters.Add("@Quantity", SqlDbType.Float).Value = Order_Item.Quantity;
                zCommand.Parameters.Add("@UnitKey", SqlDbType.Int).Value = Order_Item.UnitKey;
                zCommand.Parameters.Add("@UnitName", SqlDbType.NVarChar).Value = Order_Item.UnitName;
                zCommand.Parameters.Add("@Amount", SqlDbType.Money).Value = Order_Item.Amount;
                zCommand.Parameters.Add("@AmountDiscount", SqlDbType.Money).Value = Order_Item.AmountDiscount;
                zCommand.Parameters.Add("@PercentDiscount", SqlDbType.Float).Value = Order_Item.PercentDiscount;
                zCommand.Parameters.Add("@AmountNoVAT", SqlDbType.Money).Value = Order_Item.AmountNoVAT;
                zCommand.Parameters.Add("@VAT", SqlDbType.Float).Value = Order_Item.VAT;
                zCommand.Parameters.Add("@AmountVAT", SqlDbType.Money).Value = Order_Item.AmountVAT;
                zCommand.Parameters.Add("@AmountIncVAT", SqlDbType.Money).Value = Order_Item.AmountIncVAT;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Order_Item.Description;
                if (Order_Item.PartnerNumber != "" && Order_Item.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Order_Item.PartnerNumber);
                }
                else
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Order_Item.RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Order_Item.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Order_Item.CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Order_Item.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Order_Item.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Update()
        {
            string zSQL = "UPDATE FNC_Order_Item SET "
                        + " OrderKey = @OrderKey,"
                        + " ObjectTable = @ObjectTable,"
                        + " ObjectKey = @ObjectKey,"
                        + " ObjectName = @ObjectName,"
                        + " ObjectPrice = @ObjectPrice,"
                        + " Quantity = @Quantity,"
                        + " UnitKey = @UnitKey,"
                        + " UnitName = @UnitName,"
                        + " Amount = @Amount,"
                        + " AmountDiscount = @AmountDiscount,"
                        + " PercentDiscount = @PercentDiscount,"
                        + " AmountNoVAT = @AmountNoVAT,"
                        + " VAT = @VAT,"
                        + " AmountVAT = @AmountVAT,"
                        + " AmountIncVAT = @AmountIncVAT,"
                        + " Description = @Description,"
                        + " PartnerNumber = @PartnerNumber,"
                        + " RecordStatus = @RecordStatus,"
                        + " ModifiedOn = GetDate(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                        + " WHERE AutoKey = @AutoKey";
            string zResult = "";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Order_Item.AutoKey;
                zCommand.Parameters.Add("@OrderKey", SqlDbType.NVarChar).Value = Order_Item.OrderKey;
                zCommand.Parameters.Add("@ObjectTable", SqlDbType.NVarChar).Value = Order_Item.ObjectTable;
                zCommand.Parameters.Add("@ObjectKey", SqlDbType.NVarChar).Value = Order_Item.ObjectKey;
                zCommand.Parameters.Add("@ObjectName", SqlDbType.NVarChar).Value = Order_Item.ObjectName;
                zCommand.Parameters.Add("@ObjectPrice", SqlDbType.Money).Value = Order_Item.ObjectPrice;
                zCommand.Parameters.Add("@Quantity", SqlDbType.Float).Value = Order_Item.Quantity;
                zCommand.Parameters.Add("@UnitKey", SqlDbType.Int).Value = Order_Item.UnitKey;
                zCommand.Parameters.Add("@UnitName", SqlDbType.NVarChar).Value = Order_Item.UnitName;
                zCommand.Parameters.Add("@Amount", SqlDbType.Money).Value = Order_Item.Amount;
                zCommand.Parameters.Add("@AmountDiscount", SqlDbType.Money).Value = Order_Item.AmountDiscount;
                zCommand.Parameters.Add("@PercentDiscount", SqlDbType.Float).Value = Order_Item.PercentDiscount;
                zCommand.Parameters.Add("@AmountNoVAT", SqlDbType.Money).Value = Order_Item.AmountNoVAT;
                zCommand.Parameters.Add("@VAT", SqlDbType.Float).Value = Order_Item.VAT;
                zCommand.Parameters.Add("@AmountVAT", SqlDbType.Money).Value = Order_Item.AmountVAT;
                zCommand.Parameters.Add("@AmountIncVAT", SqlDbType.Money).Value = Order_Item.AmountIncVAT;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = Order_Item.Description;
                if (Order_Item.PartnerNumber != "" && Order_Item.PartnerNumber.Length == 36)
                {
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Order_Item.PartnerNumber);
                }
                else
                    zCommand.Parameters.Add("@PartnerNumber", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Order_Item.RecordStatus;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Order_Item.ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Order_Item.ModifiedName;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE FNC_Order_Item SET RecordStatus = 99 WHERE AutoKey = @AutoKey";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Order_Item.AutoKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Empty()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM FNC_Order_Item WHERE AutoKey = @AutoKey";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = Order_Item.AutoKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
