﻿using System;
using System.Collections.Generic;

namespace WebSite
{
    public class Order_Model
    {
        #region [ Field Name ]
        private string _OrderKey = "";
        private string _OrderID = "";
        private DateTime _OrderDate = DateTime.MinValue;
        private int _StatusOrder = 0;
        private string _StatusName = "";
        private string _Description = "";
        private string _ObjectKey = "";
        private string _ObjectName = "";
        private string _SellerKey = "";
        private string _SellerName = "";
        private string _BuyerKey = "";
        private string _BuyerName = "";
        private string _BuyerPhone = "";
        private string _BuyerAddress = "";
        private string _BuyerEmail = "";
        private string _ShippingAddress = "";
        private string _ShippingCity = "";
        private string _WarehouseKey = "";
        private string _WarehouseName = "";
        private float _TotalQuantity = 0;
        private double _AmountOrder = 0;
        private double _AmountDiscount = 0;
        private float _PercentDiscount = 0;
        private double _AmountOrderNoVAT = 0;
        private float _VAT = 0;
        private double _AmountVAT = 0;
        private double _AmountOrderIncVAT = 0;
        private double _AmountReceived = 0;
        private double _AmountReturned = 0;
        private int _CategoryKey = 0;
        private string _CategoryName = "";
        private int _Slug = 0;
        private string _CreditAccount = "";
        private string _DebitAccount = "";
        private string _PartnerNumber = "";
        private string _Organization = "";
        private int _RecordStatus = 0;
        private DateTime _CreatedOn = DateTime.MinValue;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedOn = DateTime.MinValue;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        #endregion

        #region [ Properties ]
        public string OrderKey
        {
            get { return _OrderKey; }
            set { _OrderKey = value; }
        }
        public string OrderID
        {
            get { return _OrderID; }
            set { _OrderID = value; }
        }
        public DateTime OrderDate
        {
            get { return _OrderDate; }
            set { _OrderDate = value; }
        }
        public int StatusOrder
        {
            get { return _StatusOrder; }
            set { _StatusOrder = value; }
        }
        public string StatusName
        {
            get { return _StatusName; }
            set { _StatusName = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public string ObjectKey
        {
            get { return _ObjectKey; }
            set { _ObjectKey = value; }
        }
        public string ObjectName
        {
            get { return _ObjectName; }
            set { _ObjectName = value; }
        }
        public string SellerKey
        {
            get { return _SellerKey; }
            set { _SellerKey = value; }
        }
        public string SellerName
        {
            get { return _SellerName; }
            set { _SellerName = value; }
        }
        public string BuyerKey
        {
            get { return _BuyerKey; }
            set { _BuyerKey = value; }
        }
        public string BuyerName
        {
            get { return _BuyerName; }
            set { _BuyerName = value; }
        }
        public string BuyerPhone
        {
            get { return _BuyerPhone; }
            set { _BuyerPhone = value; }
        }
        public string BuyerAddress
        {
            get { return _BuyerAddress; }
            set { _BuyerAddress = value; }
        }
        public string BuyerEmail
        {
            get { return _BuyerEmail; }
            set { _BuyerEmail = value; }
        }
        public string ShippingAddress
        {
            get { return _ShippingAddress; }
            set { _ShippingAddress = value; }
        }
        public string ShippingCity
        {
            get { return _ShippingCity; }
            set { _ShippingCity = value; }
        }
        public string WarehouseKey
        {
            get { return _WarehouseKey; }
            set { _WarehouseKey = value; }
        }
        public string WarehouseName
        {
            get { return _WarehouseName; }
            set { _WarehouseName = value; }
        }
        public float TotalQuantity
        {
            get { return _TotalQuantity; }
            set { _TotalQuantity = value; }
        }
        public double AmountOrder
        {
            get { return _AmountOrder; }
            set { _AmountOrder = value; }
        }
        public double AmountDiscount
        {
            get { return _AmountDiscount; }
            set { _AmountDiscount = value; }
        }
        public float PercentDiscount
        {
            get { return _PercentDiscount; }
            set { _PercentDiscount = value; }
        }
        public double AmountOrderNoVAT
        {
            get { return _AmountOrderNoVAT; }
            set { _AmountOrderNoVAT = value; }
        }
        public float VAT
        {
            get { return _VAT; }
            set { _VAT = value; }
        }
        public double AmountVAT
        {
            get { return _AmountVAT; }
            set { _AmountVAT = value; }
        }
        public double AmountOrderIncVAT
        {
            get { return _AmountOrderIncVAT; }
            set { _AmountOrderIncVAT = value; }
        }
        public double AmountReceived
        {
            get { return _AmountReceived; }
            set { _AmountReceived = value; }
        }
        public double AmountReturned
        {
            get { return _AmountReturned; }
            set { _AmountReturned = value; }
        }
        public int CategoryKey
        {
            get { return _CategoryKey; }
            set { _CategoryKey = value; }
        }
        public string CategoryName
        {
            get { return _CategoryName; }
            set { _CategoryName = value; }
        }
        public int Slug
        {
            get { return _Slug; }
            set { _Slug = value; }
        }
        public string CreditAccount
        {
            get { return _CreditAccount; }
            set { _CreditAccount = value; }
        }
        public string DebitAccount
        {
            get { return _DebitAccount; }
            set { _DebitAccount = value; }
        }
        public string PartnerNumber
        {
            get { return _PartnerNumber; }
            set { _PartnerNumber = value; }
        }
        public string Organization
        {
            get { return _Organization; }
            set { _Organization = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        #endregion

        public List<Order_Item_Model> ListItem = new List<Order_Item_Model>();
    }
}