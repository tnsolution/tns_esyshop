﻿namespace WebSite
{
    public class ItemCart
    {
        public string id { get; set; } = "";
        public string des { get; set; } = "";
        public float qty { get; set; } = 0;
        public double price { get; set; } = 0;
    }
}
