#pragma checksum "C:\_Project\_webbasic\Shop\Views\Shared\Components\MobileMenu.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "e3a2459aef44aebc71b61dd3bd77718ec260e2ff"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Shared_Components_MobileMenu), @"mvc.1.0.view", @"/Views/Shared/Components/MobileMenu.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\_Project\_webbasic\Shop\Views\_ViewImports.cshtml"
using WebSite;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\_Project\_webbasic\Shop\Views\_ViewImports.cshtml"
using WebSite.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"e3a2459aef44aebc71b61dd3bd77718ec260e2ff", @"/Views/Shared/Components/MobileMenu.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"b9751a11891ba42cf18e84a4b82e03210f652818", @"/Views/_ViewImports.cshtml")]
    public class Views_Shared_Components_MobileMenu : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<List<Menu_WebSite_Model>>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-controller", "Home", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Index", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 2 "C:\_Project\_webbasic\Shop\Views\Shared\Components\MobileMenu.cshtml"
  
    var ListParent = Model.Where(s => s.Parent == 0).ToList();

#line default
#line hidden
#nullable disable
            WriteLiteral(@"<div class=""mobile-menu-overlay""></div><!-- End .mobil-menu-overlay -->

<div class=""mobile-menu-container"">
    <div class=""mobile-menu-wrapper"">
        <span class=""mobile-menu-close""><i class=""icon-cancel""></i></span>
        <nav class=""mobile-nav"">

            <ul class=""mobile-menu"">
                <li class=""active"">
                    ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "e3a2459aef44aebc71b61dd3bd77718ec260e2ff4272", async() => {
                WriteLiteral("Trang chủ");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Controller = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n                </li>\r\n");
#nullable restore
#line 16 "C:\_Project\_webbasic\Shop\Views\Shared\Components\MobileMenu.cshtml"
                  
                    foreach (var parent in ListParent)
                    {
                        #region [----]
                        string parentlink = "#";
                        string parentName = parent.MenuName;
                        string parentParamater = parent.Paramater;
                        string parentAction = parent.Action;
                        int parentType = parent.TypeKey;
                        #endregion

                        var ListChild = Model.Where(s => s.Parent == parent.MenuKey).ToList();
                        if (ListChild.Count > 0)
                        {

#line default
#line hidden
#nullable disable
            WriteLiteral("                            <li>\r\n                                <a href=\"#\" class=\"nolink\">\r\n                                    ");
#nullable restore
#line 32 "C:\_Project\_webbasic\Shop\Views\Shared\Components\MobileMenu.cshtml"
                               Write(parentName);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                                </a>\r\n                                <ul class=\"submenu\">\r\n");
#nullable restore
#line 35 "C:\_Project\_webbasic\Shop\Views\Shared\Components\MobileMenu.cshtml"
                                      
                                        foreach (var child in ListChild)
                                        {
                                            #region [----]
                                            string childName = child.MenuName;
                                            string childAction = child.Action;
                                            string childParamater = child.Paramater;
                                            int childType = child.TypeKey;
                                            string childLink = "#";
                                            #endregion
                                            switch (childType)
                                            {
                                                case 1:
                                                    childLink = "/bai-viet/" + childParamater;
                                                    break;

                                                case 2:
                                                    childLink = "/tin-tuc/" + childParamater;
                                                    break;

                                                case 3:
                                                    break;

                                                case 4:
                                                    childLink = "/danh-muc/" + childParamater;
                                                    break;
                                            }

#line default
#line hidden
#nullable disable
            WriteLiteral("                                            <li>\r\n                                                <a");
            BeginWriteAttribute("href", " href=\"", 3113, "\"", 3130, 1);
#nullable restore
#line 63 "C:\_Project\_webbasic\Shop\Views\Shared\Components\MobileMenu.cshtml"
WriteAttributeValue("", 3120, childLink, 3120, 10, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">");
#nullable restore
#line 63 "C:\_Project\_webbasic\Shop\Views\Shared\Components\MobileMenu.cshtml"
                                                                Write(childName);

#line default
#line hidden
#nullable disable
            WriteLiteral("</a>\r\n                                            </li>\r\n");
#nullable restore
#line 65 "C:\_Project\_webbasic\Shop\Views\Shared\Components\MobileMenu.cshtml"
                                        }
                                    

#line default
#line hidden
#nullable disable
            WriteLiteral("                                </ul>\r\n                            </li>\r\n");
#nullable restore
#line 69 "C:\_Project\_webbasic\Shop\Views\Shared\Components\MobileMenu.cshtml"
                        }
                        else
                        {

#line default
#line hidden
#nullable disable
            WriteLiteral("                            <li>\r\n                                <a");
            BeginWriteAttribute("href", " href=\"", 3507, "\"", 3525, 1);
#nullable restore
#line 73 "C:\_Project\_webbasic\Shop\Views\Shared\Components\MobileMenu.cshtml"
WriteAttributeValue("", 3514, parentlink, 3514, 11, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">\r\n                                    <span class=\"menu-text\">\r\n                                        ");
#nullable restore
#line 75 "C:\_Project\_webbasic\Shop\Views\Shared\Components\MobileMenu.cshtml"
                                   Write(parentName);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                                    </span>\r\n                                </a>\r\n                            </li>\r\n");
#nullable restore
#line 79 "C:\_Project\_webbasic\Shop\Views\Shared\Components\MobileMenu.cshtml"
                        }
                    }
                

#line default
#line hidden
#nullable disable
            WriteLiteral(@"            </ul>
        </nav><!-- End .mobile-nav -->

        <div class=""social-icons"">
            <a href=""#"" class=""social-icon"" target=""_blank""><i class=""icon-facebook""></i></a>
            <a href=""#"" class=""social-icon"" target=""_blank""><i class=""icon-twitter""></i></a>
            <a href=""#"" class=""social-icon"" target=""_blank""><i class=""icon-instagram""></i></a>
        </div><!-- End .social-icons -->
    </div><!-- End .mobile-menu-wrapper -->
</div><!-- End .mobile-menu-container -->");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<List<Menu_WebSite_Model>> Html { get; private set; }
    }
}
#pragma warning restore 1591
