#pragma checksum "C:\_Project\_webbasic\Shop\Views\Admin\Product_List.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "3ee86f137638dfdcdf9513ae58d4f3dc6212e1dd"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Admin_Product_List), @"mvc.1.0.view", @"/Views/Admin/Product_List.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\_Project\_webbasic\Shop\Views\_ViewImports.cshtml"
using WebSite;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\_Project\_webbasic\Shop\Views\_ViewImports.cshtml"
using WebSite.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"3ee86f137638dfdcdf9513ae58d4f3dc6212e1dd", @"/Views/Admin/Product_List.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"b9751a11891ba42cf18e84a4b82e03210f652818", @"/Views/_ViewImports.cshtml")]
    public class Views_Admin_Product_List : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/theme/admin/img/noimg.png"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("alt", new global::Microsoft.AspNetCore.Html.HtmlString("product"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("rounded"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("style", new global::Microsoft.AspNetCore.Html.HtmlString("object-fit:cover; height:80px"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_4 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("btn btn-outline-primary btn-sm modal-with-zoom-anim"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_5 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-controller", "Admin", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_6 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Product_Edit", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 1 "C:\_Project\_webbasic\Shop\Views\Admin\Product_List.cshtml"
  
    Layout = "~/Views/Admin/_Layout.cshtml";

    var _ListProduct = new List<Product_Model>();
    if (ViewBag.ListProduct != null)
    {
        _ListProduct = ViewBag.ListProduct as List<Product_Model>;
    }

#line default
#line hidden
#nullable disable
            WriteLiteral(@"<header class=""page-header"">
    <h2>Danh mục sản phẩm</h2>
    <div class=""right-wrapper text-right"">
        <ol class=""breadcrumbs"">
            <li>
                <a href=""#"">
                    <i class=""fas fa-home""></i>
                </a>
            </li>
            <li></li>
        </ol>
    </div>
</header>
<div class=""row"">
    <div class=""col-md-12"">
        <div class=""card border"">
            <div class=""card-header "">
                <button type=""button"" class=""float-right btn btn-info"" id=""btn_New"">
                    <i class=""fa fa-plus""></i>
                    Thêm mới
                </button>
            </div>
            <div class=""card-body "">
");
#nullable restore
#line 33 "C:\_Project\_webbasic\Shop\Views\Admin\Product_List.cshtml"
                 if (TempData["Error"] != null && 
                       TempData["Error"].ToString() != string.Empty)
                {

#line default
#line hidden
#nullable disable
            WriteLiteral("                    <div class=\"alert alert-danger\">\r\n                        <strong>");
#nullable restore
#line 37 "C:\_Project\_webbasic\Shop\Views\Admin\Product_List.cshtml"
                           Write(TempData["Error"]);

#line default
#line hidden
#nullable disable
            WriteLiteral("</strong>.\r\n                    </div>\r\n");
#nullable restore
#line 39 "C:\_Project\_webbasic\Shop\Views\Admin\Product_List.cshtml"
                }
                else
                {
                    if (_ListProduct.Count > 0)
                    {

#line default
#line hidden
#nullable disable
            WriteLiteral(@"                        <table class=""table table-sm data"">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Hình đại diện</th>
                                    <th>Sản phẩm</th>
                                    <th>Mô tả</th>
                                    <th class=""text-right"">Giá cũ</th>
                                    <th class=""text-right"">Giá mới</th>
                                    <th></th>
                                    <th>

                                    </th>
                                </tr>
                            </thead>
                            <tbody id=""tblBody"">
");
#nullable restore
#line 60 "C:\_Project\_webbasic\Shop\Views\Admin\Product_List.cshtml"
                                 for (int i = 0; i < _ListProduct.Count; i++)
                                {
                                    #region [--Data--]
                                    var zRec = _ListProduct[i];
                                    string Key = zRec.ProductKey;
                                    string No = (i + 1).ToString();
                                    string Name = zRec.ProductName;
                                    string Description = zRec.Description;
                                    string Summarize = zRec.ProductSummarize;
                                    string Photo = zRec.PhotoPath;
                                    string NewCost = zRec.SalePrice.ToString();
                                    string OldCost = zRec.StandardCost.ToString();
                                    int CategoryKey = zRec.CategoryKey;
                                    bool Publish = zRec.Publish;
                                    #endregion

                                    

#line default
#line hidden
#nullable disable
            WriteLiteral("<tr");
            BeginWriteAttribute("id", " id=\"", 3167, "\"", 3176, 1);
#nullable restore
#line 76 "C:\_Project\_webbasic\Shop\Views\Admin\Product_List.cshtml"
WriteAttributeValue("", 3172, Key, 3172, 4, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">\r\n                                        <th>");
#nullable restore
#line 77 "C:\_Project\_webbasic\Shop\Views\Admin\Product_List.cshtml"
                                       Write(No);

#line default
#line hidden
#nullable disable
            WriteLiteral("</th>\r\n                                        <th>\r\n");
#nullable restore
#line 79 "C:\_Project\_webbasic\Shop\Views\Admin\Product_List.cshtml"
                                             if (Photo == string.Empty)
                                            {

#line default
#line hidden
#nullable disable
            WriteLiteral("                                                ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagOnly, "3ee86f137638dfdcdf9513ae58d4f3dc6212e1dd10663", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_3);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n");
#nullable restore
#line 82 "C:\_Project\_webbasic\Shop\Views\Admin\Product_List.cshtml"
                                            }
                                            else
                                            {

#line default
#line hidden
#nullable disable
            WriteLiteral("                                                <img");
            BeginWriteAttribute("src", " src=\"", 3753, "\"", 3778, 1);
#nullable restore
#line 85 "C:\_Project\_webbasic\Shop\Views\Admin\Product_List.cshtml"
WriteAttributeValue("", 3759, Url.Content(Photo), 3759, 19, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" alt=\"product\" class=\"rounded\" style=\"object-fit: cover; height: 80px\">\r\n");
#nullable restore
#line 86 "C:\_Project\_webbasic\Shop\Views\Admin\Product_List.cshtml"
                                            }

#line default
#line hidden
#nullable disable
            WriteLiteral("                                        </th>\r\n                                        <td>");
#nullable restore
#line 88 "C:\_Project\_webbasic\Shop\Views\Admin\Product_List.cshtml"
                                       Write(Name);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n                                        <td class=\"d-inline-block text-truncate\" style=\"max-width:400px\">");
#nullable restore
#line 89 "C:\_Project\_webbasic\Shop\Views\Admin\Product_List.cshtml"
                                                                                                    Write(Description);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n                                        <td class=\"text-right number\" style=\"width:100px\">");
#nullable restore
#line 90 "C:\_Project\_webbasic\Shop\Views\Admin\Product_List.cshtml"
                                                                                     Write(OldCost);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n                                        <td class=\"text-right number\" style=\"width:100px\">");
#nullable restore
#line 91 "C:\_Project\_webbasic\Shop\Views\Admin\Product_List.cshtml"
                                                                                     Write(NewCost);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n                                        <td>\r\n");
#nullable restore
#line 93 "C:\_Project\_webbasic\Shop\Views\Admin\Product_List.cshtml"
                                             if (Publish)
                                            {

#line default
#line hidden
#nullable disable
            WriteLiteral("                                                <div class=\"switch switch-sm switch-success\">\r\n                                                    <input type=\"checkbox\" name=\"switch\" data-plugin-ios-switch checked=\"checked\"");
            BeginWriteAttribute("onchange", " onchange=\"", 4712, "\"", 4739, 3);
            WriteAttributeValue("", 4723, "Activate(\'", 4723, 10, true);
#nullable restore
#line 96 "C:\_Project\_webbasic\Shop\Views\Admin\Product_List.cshtml"
WriteAttributeValue("", 4733, Key, 4733, 4, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue("", 4737, "\')", 4737, 2, true);
            EndWriteAttribute();
            WriteLiteral(" />\r\n                                                </div>\r\n");
#nullable restore
#line 98 "C:\_Project\_webbasic\Shop\Views\Admin\Product_List.cshtml"
                                            }
                                            else
                                            {

#line default
#line hidden
#nullable disable
            WriteLiteral("                                                <div class=\"switch switch-sm switch-success\">\r\n                                                    <input type=\"checkbox\" name=\"switch\" data-plugin-ios-switch");
            BeginWriteAttribute("onchange", " onchange=\"", 5151, "\"", 5178, 3);
            WriteAttributeValue("", 5162, "Activate(\'", 5162, 10, true);
#nullable restore
#line 102 "C:\_Project\_webbasic\Shop\Views\Admin\Product_List.cshtml"
WriteAttributeValue("", 5172, Key, 5172, 4, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue("", 5176, "\')", 5176, 2, true);
            EndWriteAttribute();
            WriteLiteral(" />\r\n                                                </div>\r\n");
#nullable restore
#line 104 "C:\_Project\_webbasic\Shop\Views\Admin\Product_List.cshtml"
                                            }

#line default
#line hidden
#nullable disable
            WriteLiteral("                                        </td>\r\n                                        <td>\r\n                                            ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "3ee86f137638dfdcdf9513ae58d4f3dc6212e1dd16988", async() => {
                WriteLiteral("\r\n                                                <i class=\"far fa-edit\"></i>\r\n                                            ");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_4);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Controller = (string)__tagHelperAttribute_5.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_5);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_6.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_6);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-CategoryKey", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
            }
            BeginWriteTagHelperAttribute();
#nullable restore
#line 110 "C:\_Project\_webbasic\Shop\Views\Admin\Product_List.cshtml"
                                                          WriteLiteral(CategoryKey);

#line default
#line hidden
#nullable disable
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["CategoryKey"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-CategoryKey", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["CategoryKey"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            BeginWriteTagHelperAttribute();
#nullable restore
#line 111 "C:\_Project\_webbasic\Shop\Views\Admin\Product_List.cshtml"
                                                         WriteLiteral(Key);

#line default
#line hidden
#nullable disable
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["ProductKey"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-ProductKey", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["ProductKey"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n                                            <button type=\"button\" class=\"btn btn-outline-danger btn-sm\"");
            BeginWriteAttribute("onclick", " onclick=\"", 6025, "\"", 6049, 3);
            WriteAttributeValue("", 6035, "Delete(\'", 6035, 8, true);
#nullable restore
#line 114 "C:\_Project\_webbasic\Shop\Views\Admin\Product_List.cshtml"
WriteAttributeValue("", 6043, Key, 6043, 4, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue("", 6047, "\')", 6047, 2, true);
            EndWriteAttribute();
            WriteLiteral(">\r\n                                                <i class=\"fas fa-trash\"></i>\r\n                                            </button>\r\n                                        </td>\r\n                                    </tr>\r\n");
#nullable restore
#line 119 "C:\_Project\_webbasic\Shop\Views\Admin\Product_List.cshtml"
                                }

#line default
#line hidden
#nullable disable
            WriteLiteral("                            </tbody>\r\n                        </table>\r\n");
#nullable restore
#line 122 "C:\_Project\_webbasic\Shop\Views\Admin\Product_List.cshtml"
                    }
                    else
                    {

#line default
#line hidden
#nullable disable
            WriteLiteral("                        <div class=\"alert alert-primary\">\r\n                            <strong>Bạn chưa cập nhật, hoặc không tìm thấy dữ liệu phù hợp !</strong>.\r\n                        </div>\r\n");
#nullable restore
#line 128 "C:\_Project\_webbasic\Shop\Views\Admin\Product_List.cshtml"
                    }
                }

#line default
#line hidden
#nullable disable
            WriteLiteral("            </div>\r\n            <div class=\"card-footer \">\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n");
            DefineSection("Scripts", async() => {
                WriteLiteral("\r\n    <script>\r\n        var URL_Publish = \"");
#nullable restore
#line 138 "C:\_Project\_webbasic\Shop\Views\Admin\Product_List.cshtml"
                      Write(Url.Action("Publish_Product", "Admin"));

#line default
#line hidden
#nullable disable
                WriteLiteral("\";\r\n        var URL_Delete = \"");
#nullable restore
#line 139 "C:\_Project\_webbasic\Shop\Views\Admin\Product_List.cshtml"
                     Write(Url.Action("Delete_Product", "Admin"));

#line default
#line hidden
#nullable disable
                WriteLiteral("\";\r\n        var URL_Edit = \"");
#nullable restore
#line 140 "C:\_Project\_webbasic\Shop\Views\Admin\Product_List.cshtml"
                   Write(Url.Action("Product_Edit", "Admin", new { ProductKey = 0, CategoryKey="_cate_" }));

#line default
#line hidden
#nullable disable
                WriteLiteral(@""";
    </script>
    <script>
        $(document).ready(function () {
            $(""html"").addClass(""sidebar-left-collapsed"");
            $(""#btn_New"").click(function () {
                var sPageURL = window.location.href;
                var sTmp = sPageURL.split(""/"");
                var sParam = sTmp[sTmp.length - 1];
                URL_Edit = URL_Edit.replace(""_cate_"", sParam);
                window.location = URL_Edit;
            });
        })
        function Delete(ProductKey) {
            $.confirm({
                type: ""red"",
                typeAnimated: true,
                title: ""Cảnh báo !."",
                content: ""Bạn có chắc xóa thông tin này ?."",
                buttons: {
                    confirm: {
                        text: ""Đồng ý"",
                        btnClass: ""btn-red"",
                        action: function () {
                            $.ajax({
                                url: URL_Delete,
                                type");
                WriteLiteral(@": ""POST"",
                                data: {
                                    ""ProductKey"": ProductKey
                                },
                                beforeSend: function () {
                                },
                                success: function (r) {
                                    if (r.success) {
                                        var elm = $(""#"" + ProductKey);
                                        Utils.RemoveRow(elm);
                                    }
                                    else {
                                        Utils.OpenNotify(r.message, ""Thông báo"", ""error"");
                                    }
                                },
                                error: function (err) {
                                    Utils.OpenNotify(err.responseText, ""Thông báo"", ""error"");
                                },
                                complete: function () {
                                }
   ");
                WriteLiteral(@"                         });
                        }
                    },
                    cancel: {
                        text: ""Hủy"",
                    }
                }
            });
        }
        function Activate(ProductKey) {
            $.ajax({
                url: URL_Publish,
                type: ""POST"",
                data: {
                    ""ProductKey"": ProductKey,
                },
                beforeSend: function () {

                },
                success: function (r) {
                    if (r.success) {
                        Utils.OpenNotify(""Cập nhật tình trạng thành công"", ""Thông báo"", ""success"");
                    }
                    else {
                        Utils.OpenNotify(r.message, ""Thông báo"", ""error"");
                    }
                },
                error: function (err) {
                    Utils.OpenNotify(err.responseText, ""Thông báo"", ""error"");
                },
                complete: fu");
                WriteLiteral("nction () {\r\n\r\n                }\r\n            });\r\n        }\r\n    </script>\r\n");
            }
            );
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
