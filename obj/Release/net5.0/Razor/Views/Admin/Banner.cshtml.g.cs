#pragma checksum "C:\_Project\_webbasic\Shop\Views\Admin\Banner.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "be452e9f1de86eb7d27569297c83c4031b82140c"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Admin_Banner), @"mvc.1.0.view", @"/Views/Admin/Banner.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\_Project\_webbasic\Shop\Views\_ViewImports.cshtml"
using WebSite;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\_Project\_webbasic\Shop\Views\_ViewImports.cshtml"
using WebSite.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"be452e9f1de86eb7d27569297c83c4031b82140c", @"/Views/Admin/Banner.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"b9751a11891ba42cf18e84a4b82e03210f652818", @"/Views/_ViewImports.cshtml")]
    public class Views_Admin_Banner : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("value", "1", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("value", "0", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("id", new global::Microsoft.AspNetCore.Html.HtmlString("modalEdit"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("zoom-anim-dialog modal-block modal-header-color modal-block-info mfp-hide"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_4 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("onsubmit", new global::Microsoft.AspNetCore.Html.HtmlString("return Utils.validate($(this))"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_5 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("method", "post", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_6 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("enctype", new global::Microsoft.AspNetCore.Html.HtmlString("multipart/form-data"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 1 "C:\_Project\_webbasic\Shop\Views\Admin\Banner.cshtml"
  
    Layout = "~/Views/Admin/_Layout.cshtml";

    List<Banner_Model> _ListBanner = new List<Banner_Model>();
    if (ViewBag.ListBanner != null)
    {
        _ListBanner = ViewBag.ListBanner as List<Banner_Model>;
    }

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
<header class=""page-header"">
    <h2>Các slide hình ảnh</h2>
    <div class=""right-wrapper text-right"">
        <ol class=""breadcrumbs"">
            <li>
                <a href=""#"">
                    <i class=""fas fa-home""></i>
                </a>
            </li>
            <li></li>
        </ol>
    </div>
</header>

<div class=""row"">
    <div class=""col-md-12"">
        <div class=""card border"">
            <div class=""card-header"">
                <button class=""float-right btn btn-info"" type=""button"" onclick=""Edit(0)"">
                    <i class=""fa fa-plus""></i>
                    Thêm
                </button>
            </div>

            <div class=""card-body"">
");
#nullable restore
#line 36 "C:\_Project\_webbasic\Shop\Views\Admin\Banner.cshtml"
                 if (TempData["Error"] != null &&
                       TempData["Error"].ToString() != string.Empty)
                {

#line default
#line hidden
#nullable disable
            WriteLiteral("                    <div class=\"alert alert-danger\">\r\n                        <strong>");
#nullable restore
#line 40 "C:\_Project\_webbasic\Shop\Views\Admin\Banner.cshtml"
                           Write(TempData["Error"]);

#line default
#line hidden
#nullable disable
            WriteLiteral("</strong>.\r\n                    </div>\r\n");
#nullable restore
#line 42 "C:\_Project\_webbasic\Shop\Views\Admin\Banner.cshtml"
                }

#line default
#line hidden
#nullable disable
#nullable restore
#line 43 "C:\_Project\_webbasic\Shop\Views\Admin\Banner.cshtml"
                 if (_ListBanner.Count > 0)
                {

#line default
#line hidden
#nullable disable
            WriteLiteral(@"                    <table class=""table table-sm data"">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Hình ảnh</th>
                                <th>Nội dung hiển thị kèm theo</th>
                                <th>Ẩn/ hiện</th>
                                <th class=""text-right"">

                                </th>
                            </tr>
                        </thead>
                        <tbody id=""tblBody"">
");
#nullable restore
#line 58 "C:\_Project\_webbasic\Shop\Views\Admin\Banner.cshtml"
                             for (int i = 0; i < _ListBanner.Count; i++)
                            {
                                #region [-Data-]
                                var zModel = _ListBanner[i];
                                string Key = zModel.BannerKey.ToString();
                                string No = (i + 1).ToString();
                                string Title = zModel.Title;
                                string Description = zModel.Description;
                                int Rank = zModel.Rank;
                                bool Publish = zModel.Publish;
                                #endregion

                                

#line default
#line hidden
#nullable disable
            WriteLiteral("<tr");
            BeginWriteAttribute("id", " id=\"", 2566, "\"", 2575, 1);
#nullable restore
#line 70 "C:\_Project\_webbasic\Shop\Views\Admin\Banner.cshtml"
WriteAttributeValue("", 2571, Key, 2571, 4, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">\r\n                                    <td>");
#nullable restore
#line 71 "C:\_Project\_webbasic\Shop\Views\Admin\Banner.cshtml"
                                   Write(Rank);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n                                    <td>\r\n                                        <img id=\"imgpreview\"");
            BeginWriteAttribute("src", " src=\"", 2733, "\"", 2763, 1);
#nullable restore
#line 73 "C:\_Project\_webbasic\Shop\Views\Admin\Banner.cshtml"
WriteAttributeValue("", 2739, Url.Content(zModel.Url), 2739, 24, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" alt=\"your image\" style=\"width:100px; height:50px;\" class=\"rounded-lg\" />\r\n                                    </td>\r\n                                    <td>\r\n                                        ");
#nullable restore
#line 76 "C:\_Project\_webbasic\Shop\Views\Admin\Banner.cshtml"
                                   Write(Title);

#line default
#line hidden
#nullable disable
            WriteLiteral("<br />\r\n                                        ");
#nullable restore
#line 77 "C:\_Project\_webbasic\Shop\Views\Admin\Banner.cshtml"
                                   Write(Description);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                                    </td>\r\n                                    <td>\r\n");
#nullable restore
#line 80 "C:\_Project\_webbasic\Shop\Views\Admin\Banner.cshtml"
                                         if (zModel.Publish)
                                        {

#line default
#line hidden
#nullable disable
            WriteLiteral("                                            <div class=\"switch switch-sm switch-success\">\r\n                                                <input type=\"checkbox\" name=\"switch\" data-plugin-ios-switch checked=\"checked\"");
            BeginWriteAttribute("onchange", " onchange=\"", 3438, "\"", 3463, 3);
            WriteAttributeValue("", 3449, "Activate(", 3449, 9, true);
#nullable restore
#line 83 "C:\_Project\_webbasic\Shop\Views\Admin\Banner.cshtml"
WriteAttributeValue("", 3458, Key, 3458, 4, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue("", 3462, ")", 3462, 1, true);
            EndWriteAttribute();
            WriteLiteral(" />\r\n                                            </div>\r\n");
#nullable restore
#line 85 "C:\_Project\_webbasic\Shop\Views\Admin\Banner.cshtml"
                                        }
                                        else
                                        {

#line default
#line hidden
#nullable disable
            WriteLiteral("                                            <div class=\"switch switch-sm switch-success\">\r\n                                                <input type=\"checkbox\" name=\"switch\" data-plugin-ios-switch");
            BeginWriteAttribute("onchange", " onchange=\"", 3851, "\"", 3876, 3);
            WriteAttributeValue("", 3862, "Activate(", 3862, 9, true);
#nullable restore
#line 89 "C:\_Project\_webbasic\Shop\Views\Admin\Banner.cshtml"
WriteAttributeValue("", 3871, Key, 3871, 4, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue("", 3875, ")", 3875, 1, true);
            EndWriteAttribute();
            WriteLiteral(" />\r\n                                            </div>\r\n");
#nullable restore
#line 91 "C:\_Project\_webbasic\Shop\Views\Admin\Banner.cshtml"
                                        }

#line default
#line hidden
#nullable disable
            WriteLiteral("                                    </td>\r\n                                    <td class=\"text-right\">\r\n                                        <button type=\"button\" class=\"btn btn-outline-primary btn-sm modal-with-zoom-anim\"");
            BeginWriteAttribute("onclick", " onclick=\"", 4202, "\"", 4222, 3);
            WriteAttributeValue("", 4212, "Edit(", 4212, 5, true);
#nullable restore
#line 94 "C:\_Project\_webbasic\Shop\Views\Admin\Banner.cshtml"
WriteAttributeValue("", 4217, Key, 4217, 4, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue("", 4221, ")", 4221, 1, true);
            EndWriteAttribute();
            WriteLiteral(">\r\n                                            <i class=\"far fa-edit\"></i>\r\n                                        </button>\r\n                                        <button type=\"button\" class=\"btn btn-outline-danger btn-sm\"");
            BeginWriteAttribute("onclick", " onclick=\"", 4449, "\"", 4471, 3);
            WriteAttributeValue("", 4459, "Delete(", 4459, 7, true);
#nullable restore
#line 97 "C:\_Project\_webbasic\Shop\Views\Admin\Banner.cshtml"
WriteAttributeValue("", 4466, Key, 4466, 4, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue("", 4470, ")", 4470, 1, true);
            EndWriteAttribute();
            WriteLiteral(">\r\n                                            <i class=\"fas fa-trash\"></i>\r\n                                        </button>\r\n                                    </td>\r\n                                </tr>\r\n");
#nullable restore
#line 102 "C:\_Project\_webbasic\Shop\Views\Admin\Banner.cshtml"
                            }

#line default
#line hidden
#nullable disable
            WriteLiteral("                        </tbody>\r\n                    </table>\r\n");
#nullable restore
#line 105 "C:\_Project\_webbasic\Shop\Views\Admin\Banner.cshtml"
                }
                else
                {

#line default
#line hidden
#nullable disable
            WriteLiteral("                    <div class=\"alert alert-primary\">\r\n                        <strong>Bạn chưa cập nhật, hoặc không tìm thấy dữ liệu phù hợp !</strong>.\r\n                    </div>\r\n");
#nullable restore
#line 111 "C:\_Project\_webbasic\Shop\Views\Admin\Banner.cshtml"
                }

#line default
#line hidden
#nullable disable
            WriteLiteral("            </div>\r\n            <div class=\"card-footer\">\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("form", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "be452e9f1de86eb7d27569297c83c4031b82140c15932", async() => {
                WriteLiteral(@"
    <div class=""card border border-primary"">
        <div class=""card-header"">
            <h2 class=""card-title"">Thông tin Banner </h2>
        </div>
        <div class=""card-body border-primary"">
            <div class=""form-group row"">
                <label for=""txt_Title"" class=""col-lg-5 col-xl-3 control-label text-lg-right"">Tiêu đề</label>
                <div class=""col-lg-7 col-xl-9"">
                    <input type=""text"" class=""form-control"" id=""txt_Title""");
                BeginWriteAttribute("value", " value=\"", 5891, "\"", 5899, 0);
                EndWriteAttribute();
                BeginWriteAttribute("placeholder", " placeholder=\"", 5900, "\"", 5914, 0);
                EndWriteAttribute();
                BeginWriteAttribute("required", " required=\"", 5915, "\"", 5926, 0);
                EndWriteAttribute();
                WriteLiteral(@" name=""Title"">
                </div>
            </div>
            <div class=""form-group row"">
                <label for=""txt_Description"" class=""col-lg-5 col-xl-3 control-label text-lg-right"">Diễn giải</label>
                <div class=""col-lg-7 col-xl-9"">
                    <textarea id=""txt_Description"" rows=""3"" class=""form-control"" name=""Description""></textarea>
                </div>
            </div>
            <div class=""form-group row"">
                <label for=""txt_Url"" class=""col-lg-5 col-xl-3 control-label text-lg-right"">Ảnh</label>
                <div class=""col-lg-7 col-xl-9"">
                    <input type=""file"" id=""txt_Url"" class=""form-control"" accept=""image/*;capture=camera"" name=""Banner"" />
                </div>
            </div>
            <div class=""form-group row"">
                <label for=""txt_Rank"" class=""col-lg-5 col-xl-3 control-label text-lg-right"">Thứ tự hiển thị</label>
                <div class=""col-lg-7 col-xl-9"">
                    <input t");
                WriteLiteral("ype=\"number\" class=\"form-control\" id=\"txt_Rank\"");
                BeginWriteAttribute("value", " value=\"", 6998, "\"", 7006, 0);
                EndWriteAttribute();
                BeginWriteAttribute("placeholder", " placeholder=\"", 7007, "\"", 7021, 0);
                EndWriteAttribute();
                WriteLiteral(@" name=""Rank"">
                </div>
            </div>
            <div class=""form-group row"">
                <label for=""cbo_Publish"" class=""col-lg-5 col-xl-3 control-label text-lg-right"">Ẩn/ hiện</label>
                <div class=""col-lg-7 col-xl-9"">
                    <select id=""cbo_Publish"" class=""form-control"" name=""Publish"">
                        ");
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "be452e9f1de86eb7d27569297c83c4031b82140c18930", async() => {
                    WriteLiteral("Hiển thị");
                }
                );
                __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
                __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper.Value = (string)__tagHelperAttribute_0.Value;
                __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
                BeginWriteTagHelperAttribute();
                __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
                __tagHelperExecutionContext.AddHtmlAttribute("selected", Html.Raw(__tagHelperStringValueBuffer), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.Minimized);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                WriteLiteral("\r\n                        ");
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "be452e9f1de86eb7d27569297c83c4031b82140c20503", async() => {
                    WriteLiteral("Ẩn");
                }
                );
                __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
                __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper.Value = (string)__tagHelperAttribute_1.Value;
                __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                WriteLiteral(@"
                    </select>
                </div>
            </div>
            <div class=""card-footer border-primary text-center"">
                <button id=""btn_Cancel"" type=""button"" class=""btn btn-outline-secondary modal-dismiss"">
                    <i class=""far fa-times-circle""></i>
                    Đóng
                </button>
                <button id=""btn_Save"" type=""submit"" class=""btn btn-outline-primary modal-confirm"">
                    <i class=""far fa-save""></i>
                    Cập nhật
                </button>
            </div>
        </div>
        <input id=""txt_BannerKey"" type=""hidden"" name=""BannerKey"" value=""0"" />
    </div>
");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_3);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_4);
            BeginAddHtmlAttributeValues(__tagHelperExecutionContext, "action", 1, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
#nullable restore
#line 120 "C:\_Project\_webbasic\Shop\Views\Admin\Banner.cshtml"
AddHtmlAttributeValue("", 5322, Url.Action("Save_Banner","Admin"), 5322, 34, false);

#line default
#line hidden
#nullable disable
            EndAddHtmlAttributeValues(__tagHelperExecutionContext);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Method = (string)__tagHelperAttribute_5.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_5);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_6);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n");
            DefineSection("scripts", async() => {
                WriteLiteral("\r\n    <script>\r\n        var URL_Edit = \"");
#nullable restore
#line 176 "C:\_Project\_webbasic\Shop\Views\Admin\Banner.cshtml"
                   Write(Url.Action("Get_Banner", "Admin"));

#line default
#line hidden
#nullable disable
                WriteLiteral("\";\r\n        var URL_Delete = \"");
#nullable restore
#line 177 "C:\_Project\_webbasic\Shop\Views\Admin\Banner.cshtml"
                     Write(Url.Action("Delete_Banner", "Admin"));

#line default
#line hidden
#nullable disable
                WriteLiteral("\";\r\n        var URL_Save = \"");
#nullable restore
#line 178 "C:\_Project\_webbasic\Shop\Views\Admin\Banner.cshtml"
                   Write(Url.Action("Save_Banner", "Admin"));

#line default
#line hidden
#nullable disable
                WriteLiteral("\";\r\n        var URL_Active = \"");
#nullable restore
#line 179 "C:\_Project\_webbasic\Shop\Views\Admin\Banner.cshtml"
                     Write(Url.Action("Activate_Banner", "Admin"));

#line default
#line hidden
#nullable disable
                WriteLiteral(@""";
    </script>
    <script>
        $(document).ready(function () {

        });
        
        function Edit(BannerKey) {
            Utils.OpenMagnific(""#modalEdit"");
            if (BannerKey > 0) {
                $.ajax({
                    url: URL_Edit,
                    type: ""GET"",
                    data: {
                        ""BannerKey"": BannerKey
                    },
                    beforeSend: function () {
                        Utils.LoadIn();
                    },
                    success: function (r) {
                        if (r.success) {
                            var obj = JSON.parse(r.data);
                            $(""#txt_BannerKey"").val(obj.BannerKey);
                            $(""#txt_Title"").val(obj.Title);
                            $(""#txt_Description"").val(obj.Description);
                            $(""#txt_Rank"").val(obj.Rank);
                            var zPublish = 0;
                            if (obj.Publish ");
                WriteLiteral(@"== true)
                                zPublish = 1;
                            else
                                zPublish = 0;
                            $(""#cbo_Publish"").val(zPublish).trigger(""change"");
                        }
                        else {
                            Utils.OpenNotify(r.message, ""Thông báo"", ""error"");
                        }
                    },
                    error: function (err) {
                        Utils.OpenNotify(err.responseText, ""Thông báo"", ""error"");
                    },
                    complete: function () {
                        Utils.LoadOut();
                    }
                });
            }
            else {
                Utils.ClearUI(""#modalEdit"");
                $(""#cbo_Rank"").val(0);
                $(""#txt_BannerKey"").val(0);
                $(""#cbo_Publish"").val(1);
            }
        }
        function Delete(BannerKey) {
            $.confirm({
                type: ""red"",
     ");
                WriteLiteral(@"           typeAnimated: true,
                title: ""Cảnh báo !."",
                content: ""Bạn có chắc xóa thông tin này ?."",
                buttons: {
                    confirm: {
                        text: ""Đồng ý"",
                        btnClass: ""btn-red"",
                        action: function () {
                            $.ajax({
                                url: URL_Delete,
                                type: ""POST"",
                                data: {
                                    ""BannerKey"": BannerKey
                                },
                                beforeSend: function () {

                                },
                                success: function (r) {
                                    if (r.success) {
                                        var elem = $(""#"" + BannerKey);
                                        Utils.RemoveRow(elem);
                                    }
                                    else {");
                WriteLiteral(@"
                                        Utils.OpenNotify(r.message, ""Thông báo"", ""error"");
                                    }
                                },
                                error: function (err) {
                                    Utils.OpenNotify(err.responseText, ""Thông báo"", ""error"");
                                },
                                complete: function () {

                                }
                            });
                        }
                    },
                    cancel: {
                        text: ""Hủy"",
                    }
                }
            });
        }
        function Activate(BannerKey) {
            $.ajax({
                url: URL_Active,
                type: ""POST"",
                data: {
                    ""BannerKey"": BannerKey,
                },
                beforeSend: function () {

                },
                success: function (r) {
                    if (r.s");
                WriteLiteral(@"uccess) {
                        Utils.OpenNotify(""Cập nhật tình trạng thành công"", ""Thông báo"", ""success"");
                    }
                    else {
                        Utils.OpenNotify(r.message, ""Thông báo"", ""error"");
                    }
                },
                error: function (err) {
                    Utils.OpenNotify(err.responseText, ""Thông báo"", ""error"");
                },
                complete: function () {

                }
            });
        }
    </script>
");
            }
            );
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
