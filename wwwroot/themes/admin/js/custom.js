﻿$(".se-pre-con").fadeOut("slow");
if (localStorage.getItem('url') !== null) {
    var url = localStorage.getItem('url');
    $('a[href="' + url + '"]').addClass('active')
        .closest('li')
        .parents('li.nav-parent')
        .addClass('nav-expanded');
}
else {
    var pathname = document.location.pathname;
    $('#sidebar-left a').each(function () {
        var value = jQuery(this).attr('href');
        if (pathname.indexOf(value) > -1) {
            $(this).addClass('active')
                .closest('li')
                .parents('li.nav-parent')
                .addClass('nav-expanded');
            return false;
        }
    });
}
$("#sidebar-left").on("click", "a", function () {
    var link = $(this).attr("href");
    if (link !== "#") {
        $(".se-pre-con").fadeIn("slow");
        localStorage.setItem("url", link);
    }
});
$(".select2").select2({
    width: "100%",
    placeholder: "--Chọn--",
    tags: true,
    createTag: function (params) {
        return {
            id: params.term,
            text: params.term,
            newOption: true
        };
    }
});
$(".datepicker").datepicker({
    todayHighlight: true,
    autoclose: true,
    format: "dd/mm/yyyy"
});
$("div[data-plugin-datepicker]").datepicker({
    todayHighlight: true,
    format: "dd/mm/yyyy",
}).on("changeDate", function (e) {
    var date = $(this).datepicker("getFormattedDate");
    var attr = $(this).attr("name");
    $("#txt_" + attr).val(date);
});
$(document).on("click", ".modal-dismiss", function (e) {
    e.preventDefault();
    $.magnificPopup.close();
    $('.error').remove();
});
$(document).on("click", ".modal-confirm", function (e) {
    if ($(this).attr("type") === "submit") {
        return;
    }
    e.preventDefault();
    $.magnificPopup.close();
});
$(".number").each(function () {
    $(this).number($(this).text(), 0, ',', '.');
});
sessionTimeout({
    warnAfter: 3600000,
    titleText: "Thông báo.",
    message: "Phiên làm việc của bạn sắp hết thời gian, phần mềm sẽ quay về trang đăng nhập trong lần thao tác kế tiếp !.",
    keepAliveUrl: "/Base/KeepSessionAlive",
    logOutUrl: "/Auth/SignIn",
    timeOutUrl: "/Auth/SignIn",
    logOutBtnText: "Kết thúc phiên làm việc",
    stayConnectedBtnText: "Giữ phiên làm việc"
});