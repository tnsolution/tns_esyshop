﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace WebSite.Controllers
{
    public class MemberController : Controller
    {
        public IActionResult Login()
        {
            if (Request.Cookies["member"] != null)
            {
                var auth = Request.Cookies["member"];
                var obj = JsonConvert.DeserializeObject<Auth>(auth);
                ViewBag.UserName = obj.UserName;
                ViewBag.Password = obj.Password;
            }

            return View();
        }

        public IActionResult Login_Normal(string Email, string Password)
        {
            Email ??= "";
            Password ??= "";

            Customer_Info zInfo;
            if (Password.Length != 28)
            {
                zInfo = new Customer_Info(Email, TN_Utils.HashPass(Password), true);
            }
            else
            {
                zInfo = new Customer_Info(Email, Password, true);
            }

            if (zInfo.Code == "200")
            {
                var key = "MemberLogged";
                var obj = JsonConvert.SerializeObject(zInfo.Customer);
                HttpContext.Session.SetString(key, obj);

                ViewBag.Message = "IsAuthen";

                //Update Logged
                zInfo.UpdateLogged();

                //Cookies login
                Response.Cookies.Append("member", JsonConvert.SerializeObject(new Auth
                {
                    ID = zInfo.Customer.CustomerKey,
                    UserName = Email,
                    Password = TN_Utils.HashPass(Password)
                }));
            }

            return RedirectToAction("Index", "Home");
        }

        public IActionResult Register(string RegEmail, string RegPassword)
        {
            RegEmail ??= "";
            RegPassword ??= "";

            var jResult = ExistMember(RegEmail).Value as ServerResult;
            if (!jResult.Success)
            {
                var zInfo = new Customer_Info();
                zInfo.Customer.Email = RegEmail;
                zInfo.Customer.Password = TN_Utils.HashPass(RegPassword);
                zInfo.Customer.PartnerNumber = TN_Helper.PartnerNumber;
                zInfo.Customer.Referrer = UriHelper.GetDisplayUrl(Request);
                zInfo.Create_ServerKey();
            }
            else
            {

            }

            return View();
        }

        public JsonResult ExistMember(string UserName)
        {
            var zResult = new ServerResult();
            var zInfo = new Customer_Info(UserName, true);
            if (zInfo.Code == "200")
            {
                zResult.Success = true;
                zResult.Message = "Tài khoản này đã có rồi vui lòng nhâp lại !.";
            }
            else
            {
                zResult.Success = false;
            }

            return Json(zResult);
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult AddToCart(string ProductKey)
        {
            var zResult = new ServerResult();
            try
            {
                string sOrder = HttpContext.Session.GetString("Order");

                Order_Model zOrder;
                if (!string.IsNullOrEmpty(sOrder))
                {
                    zOrder = JsonConvert.DeserializeObject<Order_Model>(sOrder);
                }
                else
                {
                    zOrder = new Order_Model();
                }

                Order_Item_Model zItem = zOrder.ListItem.SingleOrDefault(s => s.ObjectKey == ProductKey);
                if (zItem != null)
                {
                    zItem.Quantity++;
                    zItem.Amount = zItem.Quantity * zItem.ObjectPrice;
                }
                else
                {
                    zItem = new Order_Item_Model();
                    var Product = new Product_Info(ProductKey).Product;

                    zItem.ObjectKey = Product.ProductKey;
                    zItem.ObjectName = Product.ProductName;
                    zItem.ObjectPrice = Product.SalePrice;
                    zItem.Quantity = 1;
                    zItem.UnitKey = Product.StandardUnitKey;
                    zItem.UnitName = Product.StandardUnitName;
                    zItem.Amount = Product.SalePrice;
                    zItem.PhotoPath = Product.PhotoPath;

                    zOrder.ListItem.Add(zItem);
                }

                HttpContext.Session.SetString("Order", JsonConvert.SerializeObject(zOrder));
                zResult.Success = true;
            }
            catch (Exception ex)
            {
                zResult.Success = false;
                zResult.Message = ex.ToString().GetFirstLine();
            }

            return Json(zResult);
        }
        [HttpPost]
        public JsonResult RemoveCart(string ProductKey)
        {
            var zResult = new ServerResult();
            try
            {
                string sOrder = HttpContext.Session.GetString("Order");
                var zOrder = JsonConvert.DeserializeObject<Order_Model>(sOrder);
                var zItem = zOrder.ListItem.SingleOrDefault(s => s.ObjectKey == ProductKey);
                if (zItem != null && zItem.Quantity > 1)
                {
                    zItem.Quantity--;
                }
                else
                {
                    zOrder.ListItem.Remove(zItem);
                }

                HttpContext.Session.SetString("Order", JsonConvert.SerializeObject(zOrder));
                zResult.Success = true;
            }
            catch (Exception ex)
            {
                zResult.Message = ex.ToString().GetFirstLine();
                zResult.Success = false;
            }
            return Json(zResult);
        }

        [Route("gio-hang")]
        public IActionResult Cart()
        {
            Order_Model zOrder;
            try
            {
                string sOrder = HttpContext.Session.GetString("Order");

                if (!string.IsNullOrEmpty(sOrder))
                {
                    zOrder = JsonConvert.DeserializeObject<Order_Model>(sOrder);
                }
                else
                {
                    zOrder = new Order_Model();
                }


            }
            catch (Exception ex)
            {

                throw;
            }

            return View(zOrder);
        }

        [HttpPost]
        public JsonResult CheckOut(List<ItemCart> data)
        {
            var zResult = new ServerResult();
            string sOrder = HttpContext.Session.GetString("Order");
            if (!string.IsNullOrEmpty(sOrder))
            {
                var zOrder = JsonConvert.DeserializeObject<Order_Model>(sOrder);
                var zList = zOrder.ListItem;

                foreach (var rec in data)
                {
                    var id = rec.id;
                    var amount = rec.qty * rec.price;
                    var des = rec.des;
                    var zItem = zList.Single(s => s.ObjectKey == id);
                    zItem.Quantity = rec.qty;
                    zItem.Amount = amount;
                    zItem.Description = des;
                }

                zOrder.ListItem = zList;
                HttpContext.Session.SetString("Order", JsonConvert.SerializeObject(zOrder));
                zResult.Success = true;
            }
            else
            {
                zResult.Message = "Session is null";
                zResult.Success = false;
            }

            return Json(zResult);
        }
        [HttpPost]
        public JsonResult CancelCart()
        {
            var zResult = new ServerResult();
            zResult.Success = true;
            HttpContext.Session.Clear();
            return Json(zResult);
        }

        [Route("giao-hang")]
        [HttpGet]
        public IActionResult ShippingCart()
        {
            string sOrder = HttpContext.Session.GetString("Order");
            var zOrder = new Order_Model();
            if (!string.IsNullOrEmpty(sOrder))
            {
                zOrder = JsonConvert.DeserializeObject<Order_Model>(sOrder);
            }
            return View(zOrder);
        }

        [HttpPost]
        public IActionResult SaveOrder(string Name, string Phone, string Address, string Description)
        {
            string Key = Guid.NewGuid().ToString();
            string ID = Order_Data.AutoID("ES", TN_Helper.PartnerNumber);
            bool Error = false;
            string Message = "";
            try
            {
                string sOrder = HttpContext.Session.GetString("Order");
                var zOrder = new Order_Model();
                if (!string.IsNullOrEmpty(sOrder))
                {
                    zOrder = JsonConvert.DeserializeObject<Order_Model>(sOrder);
                    zOrder.StatusOrder = 1;
                    zOrder.StatusName = "Chờ xác nhận";
                    zOrder.OrderID = ID;
                    zOrder.OrderKey = Key;
                    zOrder.BuyerName = Name;
                    zOrder.BuyerPhone = Phone;
                    zOrder.BuyerAddress = Address;
                    zOrder.Description = Description;
                    zOrder.OrderDate = DateTime.Now;
                    zOrder.PartnerNumber = TN_Helper.PartnerNumber;
                    zOrder.AmountOrder = zOrder.ListItem.Sum(s => s.Amount);
                    zOrder.CreatedBy = TN_Helper.BusinessName;
                    zOrder.CreatedName = TN_Helper.BusinessName;
                    zOrder.ModifiedBy = TN_Helper.BusinessName;
                    zOrder.ModifiedName = TN_Helper.BusinessName;
                    zOrder.ClearNullable();
                    var zInfo = new Order_Info
                    {
                        Order = zOrder
                    };
                    zInfo.Create_ClientKey();

                    if (zInfo.Code == "200" ||
                        zInfo.Code == "201")
                    {
                        foreach (var rec in zOrder.ListItem)
                        {
                            rec.OrderKey = Key;
                            rec.ClearNullable();

                            var ItemInfo = new Order_Item_Info();
                            ItemInfo.Order_Item = rec;
                            ItemInfo.Create_ServerKey();
                            if (ItemInfo.Code != "200" &&
                                ItemInfo.Code != "201")
                            {
                                Message = "Lưu sản phẩm không được !. [" + ItemInfo.Message + "]";
                                Error = true;
                                break;
                            }
                        }
                    }
                    else
                    {
                        Message = "Lưu đơn hàng không được !. [" + zInfo.Message + "]";
                        Error = true;
                    }
                }
            }
            catch (Exception ex)
            {
                TempData["Error"] = "Lỗi khi đặt hàng vui lòng liên hệ hotline, chúng tôi sẽ đặt hàng giúp bạn !. [" + ex.ToString().GetFirstLine() + "]";
                Error = true;
            }

            if (Error)
            {
                TempData["Error"] = "Lỗi khi đặt hàng vui lòng liên hệ hotline, chúng tôi sẽ đặt hàng giúp bạn !.[" + Message + "]";
                TN_Helper.SendMessage("Có đơn hàng vừa đặt: [" + TempData["Error"] + "]");
                return RedirectToAction("ShippingCart");
            }
            else
            {
                TN_Helper.SendMessage("Có đơn hàng vừa đặt: [" + ID + "] lúc "+DateTime.Now.ToString("dd/MM HH:mm"));
                return RedirectToAction("ReviewOrder", new { OrderKey = Key });
            }
        }
        [Route("don-hang-cua-ban/{OrderKey}")]
        public IActionResult ReviewOrder(string OrderKey)
        {
            HttpContext.Session.Clear();

            var zInfo = new Order_Info(OrderKey);
            var zModel = zInfo.Order;
            zModel.ListItem = Order_Data.ListDetail(OrderKey, out _);
            return View(zModel);
        }
    }
}