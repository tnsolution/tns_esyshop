﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebSite.Controllers
{
    public class AuthController : Controller
    {

        [Route("dang-nhap")]
        public IActionResult Index()
        {
            if (Request.Cookies["auth"] != null)
            {
                var auth = Request.Cookies["auth"];
                var obj = JsonConvert.DeserializeObject<Auth>(auth);
                ViewBag.UserName = obj.UserName;
                ViewBag.Password = obj.Password;
            }

            ViewBag.Message = "NotAuthen";
            return View();
        }
        public IActionResult SignIn(string UserName, string Password)
        {
            if (Password.Length < 28)
            {
                Password = TN_Utils.HashPass(Password);
            }

            var zInfo = new User_Info(UserName, Password);
            if (zInfo.Code == "200")
            {
                var key = "UserLogged";
                var obj = JsonConvert.SerializeObject(zInfo.User);
                HttpContext.Session.SetString(key, obj);

                ViewBag.Message = "IsAuthen";

                //Update Logged
                zInfo.UpdateLogged();

                //Cookies login
                Response.Cookies.Append("auth", JsonConvert.SerializeObject(new Auth
                {
                    UserName = UserName,
                    Password = Password
                }));

                //Logo Admin
                var Logo = new Partner_Info(zInfo.User.PartnerNumber).Partner.Logo_Large;
                Response.Cookies.Append("logo", Logo);
            }
            else
            {
                ViewBag.Message = "405";
            }

            return View("~/Views/Auth/Index.cshtml");
        }
    }
}
