﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace WebSite.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IConfiguration _Iconfiguration;

        public HomeController(ILogger<HomeController> logger, IConfiguration iconfiguration)
        {
            _logger = logger;
            _Iconfiguration = iconfiguration;

            TN_Helper.ConnectionString = _Iconfiguration.GetSection("Data").GetSection("ConnectionString").Value;
            TN_Helper.PartnerNumber = _Iconfiguration.GetSection("PartnerNumber").Value;
        }

        public IActionResult Index()
        {
            //dùng lấy các sản phẩm mới hiển thị ra group chạy slide trên trang chủ
            ViewBag.SessionFront = new ParamProductComponent() { GroupName = "Sản phẩm mới", GroupProduct = "NEW", Category = 0 };


            return View();
        }

        #region [--tin tuc--]

        [Route("bai-viet/{ArticleKey}")]
        public IActionResult Article(string ArticleKey)
        {
            var zInfo = new Article_Info(ArticleKey);
            if (zInfo.Code == "404" ||
                zInfo.Article.Publish == false ||
                zInfo.Article.RecordStatus == 99)
            {
                return View("~/Views/Shared/404.cshtml");
            }

            ViewBag.CategoryName = new Article_Category_Info(zInfo.Article.CategoryKey).Article_Category.CategoryName;

            return View("~/Views/Home/Article.cshtml", zInfo.Article);
        }

        [Route("tin-tuc/{CategoryKey}")]
        public IActionResult Article_List(int CategoryKey)
        {
            ViewBag.CategoryName = new Article_Category_Info(CategoryKey).Article_Category.CategoryName;
            var zList = Article_Data.ListShow(TN_Helper.PartnerNumber, CategoryKey, out _);
            return View("~/Views/Home/Article_List.cshtml", zList);
        }

        #endregion

        [Route("lien-he")]
        public IActionResult Contact()
        {
            return View();
        }

        #region [--san pham--]
        [Route("danh-muc/{CategoryKey}")]
        public IActionResult Product_List(int CategoryKey)
        {
            var zCategory = new Product_Category_Info(CategoryKey).Product_Category;
            ViewBag.CategoryName = zCategory.CategoryName;
            ViewBag.CategoryKey = zCategory.CategoryKey;
            var zList = Product_Data.ListShow(TN_Helper.PartnerNumber, string.Empty, CategoryKey, out _);

            if (zList.Count <= 0)
            {
                var childcate = Product_Category_Data.List(TN_Helper.PartnerNumber, CategoryKey, out _);
                if (childcate.Count > 0)
                {
                    string joined = "";
                    foreach (var c in childcate)
                    {
                        joined += c.CategoryKey + ",";
                    }
                    joined = joined.Remove(joined.LastIndexOf(","), 1);
                    zList = Product_Data.ListShow(TN_Helper.PartnerNumber, joined, out _);
                }
            }

            return View("~/Views/Home/Product_List.cshtml", zList);
        }
        [Route("mat-hang/{ProductID}")]
        public IActionResult Product_View(string ProductID)
        {
            var zInfo = new Product_Info();
            zInfo.Get_Product_ID(ProductID);
            if (zInfo.Code == "404" ||
                zInfo.Product.Publish == false ||
                zInfo.Product.RecordStatus == 99)
            {
                return View("~/Views/Shared/404.cshtml");
            }

            var ListPhoto = Product_Photo_Data.List(zInfo.Product.ProductKey, out _);
            zInfo.Product.PhotoList = JsonConvert.SerializeObject(ListPhoto);
            ViewBag.SessionRelated = new ParamProductComponent() { GroupName = "Sản phẩm cùng loại", GroupProduct = "", Category = zInfo.Product.CategoryKey };
            return View("~/Views/Home/Product_View.cshtml", zInfo.Product);
        }
        #endregion

        [Route("xem-nhanh/{ProductID}")]
        [HttpGet]
        public IActionResult Product_Quick(string ProductID)
        {
            var zInfo = new Product_Info();
            zInfo.Get_Product_ID(ProductID);
            var ListPhoto = Product_Photo_Data.List(zInfo.Product.ProductKey, out _);

            zInfo.Product.PhotoList = JsonConvert.SerializeObject(ListPhoto);
            if (zInfo.Code == "404" ||
               zInfo.Product.Publish == false ||
               zInfo.Product.RecordStatus == 99)
            {
                return View("~/Views/Shared/404.cshtml");
            }
            return View("~/Views/Home/Product_Quick.cshtml", zInfo.Product);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View("~/Views/Shared/404.cshtml");
            //return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [HttpPost]
        public JsonResult SendMail(string Email, string Name, string Title, string Content)
        {
            var zResult = new ServerResult();
            if (Email == null && Email == string.Empty)
            {
                zResult.Success = false;
                zResult.Message = "Mail không đúng định dạng !.";
                return Json(zResult);
            }

            Name ??= "";
            Title ??= "";
            Content ??= "";

            var zModel = new EmailModel
            {
                Email = _Iconfiguration.GetSection("MailInfo").GetSection("MailFrom").Value,
                Password = _Iconfiguration.GetSection("MailInfo").GetSection("Password").Value,
                To = _Iconfiguration.GetSection("MailInfo").GetSection("MailTo").Value,
            };

            if (Name != string.Empty &&
                Title != string.Empty &&
                Content != string.Empty)
            {
                string Body =
                   "Họ tên:" + Name + "<br/>" +
                   "Email: " + Email + "<br/>" +
                   "Nội dung:" + Content + "<br/>";

                zModel.Subject = Title;
                zModel.Body = Body;
            }
            else
            {
                Title = "Nhận đăng ký thông tin";
                string Body = "Nhận thông tin gửi đến mail:" + Email;
                zModel.Subject = Title;
                zModel.Body = Body;
            }

            zResult.Message = TN_Utils.SendGmail(zModel);
            return Json(zResult);
        }
    }
}